Peans
=======
    java Web项目的脚手架项目，基于spring-boot,汇聚最流行最新的java框架，是作者多年编程经验的总结，是作者思想的实践。 
## Peans名字的含义？ ##
    作者的小土狗的昵称的英文名，它叫豆豆
       
## Peans是做什么的？ ##
    本质上它是java 后台接口服务的脚手架，它是作者的多年编程设计经验的总结，它把一些流行的框架融入其中。
    它是基于spring-boot的，它的目标是致力于新项目的快速是搭建，投产使用等，它来源于作者的开发实践。
    它的内容广泛，它涉及多个框架，它努力寻找一条最能提高开发效率的框架。
    
## 编写Peans的原因？ ##
    在Java混迹多年，从最初的毛头小伙到现在30而立，已过了好几个年头，在中国软件的环境里已称为老人，想想自己做技术那么多年，也
    该写点什么，于是就萌生了写点什么的愿望。
    Peans刚开始写的是时候是spring-boot还是1.2的时候，那时就预感到微服务和动静分离是以后趋势所在，由于受jfinal的影响，追求
    开发效率极致，所以就试着把spring-boot做得更朝那个方向走。我是一个喜欢用别人的轮子的人，更注重实际产出的，所以就萌生写一个
    整合多种框架的一种集大成的框架，peans应运而生。
    
## Peans的特点 ##

- 基于spring-boot,并对spring-boot进行补充
- 基于动静分离设计，静态部分的框架是[Peans静态前端框架项目](http://git.oschina.net/duduFeng/peans-html)
- 包含一些分布式框架使用，如dubbo,disconf,diamond
- 遵循约定大于配置，倾向于用实践来代替硬性规范

## Peans模块 ##

- peans-core:peans核心类
- peans-diamond-starter:对阿里钻石配置的封装
- peans-disconf-client:是对disconf-client包的补充，disconf-client扩展性不好，还有些bug，所以创建了些项目
- peans-disconf-starter:对百度配置中心的封装
- penas-dubbo-starter:对dubbo配置的封装
- peans-swagger2-starter:对swagger2配置的封装
- peans-web-starter:对web配置的封装
- peans-sample:使用peans的样例封装，脚手架项目