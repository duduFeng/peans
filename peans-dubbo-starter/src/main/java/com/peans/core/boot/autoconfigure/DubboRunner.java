package com.peans.core.boot.autoconfigure;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;

/**
 * Created by admin on 2016/9/20.
 */
public class DubboRunner implements CommandLineRunner {

    protected final Logger logger = LoggerFactory.getLogger(DubboRunner.class);

    @Override
    public void run(String... args) throws Exception {
        System.out.println("dubbo自动配置启动成功.......");
        logger.info("dubbo自动配置启动成功.......");
    }
}
