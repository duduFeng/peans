package com.peans.core.boot.autoconfigure;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

/**
 * Created by admin on 2016/9/20.
 */
@Configuration
@ImportResource("classpath*:dubbo-config.xml")
@EnableConfigurationProperties(DubboProperties.class)
public class DubboAutoConfiguration {

}
