package com.peans.core.boot.autoconfigure;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Created by admin on 2016/9/20.
 */
@ConfigurationProperties(prefix = "peans.dubbo")
public class DubboProperties{

    private String applicationName;

    private String providerDelay;

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public String getProviderDelay() {
        return providerDelay;
    }

    public void setProviderDelay(String providerDelay) {
        this.providerDelay = providerDelay;
    }
}
