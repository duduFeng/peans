package com.peans.sample.provider.impl;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.peans.core.protocol.PageQueryVo;
import com.peans.core.protocol.RspListVo;
import com.peans.core.protocol.RspVo;
import com.peans.core.protocol.RspVoFactory;
import com.peans.sample.protocol.AddUserEo;
import com.peans.sample.protocol.EditUserEo;
import com.peans.sample.protocol.QueryUserEo;
import com.peans.sample.protocol.UserVo;
import com.peans.sample.service.UserService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * Created by nickfeng on 2017/5/25.
 */
@Service
public class UserServiceImpl implements UserService {

    private static Map<Integer,UserVo> resultMap =Maps.newHashMap();

    @Override
    public RspVo<Void> addUser(AddUserEo eo) {
        UserVo vo = new UserVo();
        BeanUtils.copyProperties(eo,vo);
        resultMap.put(vo.getId(),vo);
        return RspVoFactory.success(vo);
    }

    @Override
    public RspVo<Void> editUser(EditUserEo eo) {
        UserVo vo = new UserVo();
        BeanUtils.copyProperties(eo,vo);
        resultMap.put(vo.getId(),vo);
        return RspVoFactory.success(vo);
    }

    @Override
    public RspVo<UserVo> queryById(QueryUserEo eo) {
        return RspVoFactory.success(resultMap.get(eo.getId()));
    }

    @Override
    public RspListVo<UserVo> pageQuery(QueryUserEo eo) {
        PageQueryVo<UserVo> result = new PageQueryVo<>();
        result.setTotal(resultMap.size());
        List<UserVo> list = Lists.newArrayList();
        for (Map.Entry<Integer,UserVo> entry : resultMap.entrySet()) {
            list.add(entry.getValue());
        }
        result.setContent(list);
        return RspVoFactory.successPage(result);
    }
}
