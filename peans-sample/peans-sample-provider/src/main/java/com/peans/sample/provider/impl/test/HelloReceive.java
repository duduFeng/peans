package com.peans.sample.provider.impl.test;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@RabbitListener(queues = "hello")
public class HelloReceive {

    @RabbitHandler
    public void processC(String str) {
        System.out.println("xxxx");
        System.out.println("Receive:"+str);
    }

}