package com.peans.sample;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.amqp.core.Queue;
import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
//@ImportResource("classpath*:dubbo-provide.xml")
public class WebConfig extends SpringBootServletInitializer {
	
	private static final Log logger = LogFactory.getLog(WebConfig.class);

	
	//@Autowired
	//private DataSourceProperties properties;
	
	/**
	@Bean
	@ConfigurationProperties(prefix = DataSourceProperties.PREFIX)
	public DataSource druidDataSource(){
		DruidDataSource dataSource = new DruidDataSource();
		dataSource.setDriverClassName(properties.getDriverClassName());
		dataSource.setUrl(properties.getUrl());
		dataSource.setUsername(properties.getUsername());
		dataSource.setPassword(properties.getPassword());
		dataSource.setMinIdle(1);
		try {
			dataSource.setFilters("stat");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return dataSource;
	}
	**/
	
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		//application.web(true);
		application.bannerMode(Banner.Mode.CONSOLE);
		return application.sources(WebConfig.class);
	}

	public static void main(String[] args) throws Exception {
		SpringApplication app = new SpringApplication(WebConfig.class);
		//app.setBannerMode(Banner.Mode.CONSOLE);
		//app.setWebEnvironment(true);
		app.run(args);
	}

	@Bean
	public Queue queue() {
		return new Queue("queue");
	}

}
