package com.peans.sample.provider.test;

import com.peans.sample.WebConfig;
import com.peans.sample.provider.impl.test.HelloSender;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest(classes=WebConfig.class)
@RunWith(SpringRunner.class)
//@SpringBootTest
public class RabbitMqTest {

    @Autowired
    private HelloSender helloSender;

    @Autowired
    private AmqpTemplate template;

    @Test
    public void aa() {
        System.out.println(template);
        helloSender.send();
    }

    public static void main(String[] args) {

    }
}
