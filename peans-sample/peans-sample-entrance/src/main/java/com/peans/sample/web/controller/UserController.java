package com.peans.sample.web.controller;

import com.peans.core.protocol.RspListVo;
import com.peans.core.protocol.RspVo;
import com.peans.core.web.BaseApiController;
import com.peans.sample.protocol.AddUserEo;
import com.peans.sample.protocol.EditUserEo;
import com.peans.sample.protocol.QueryUserEo;
import com.peans.sample.protocol.UserVo;
import com.peans.sample.service.UserService;
import com.peans.sample.web.component.BusConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by nickfeng on 2017/5/25.
 */
@RestController
public class UserController extends BaseApiController {

    public final String PATH_PREFIX = "user.";

    @Autowired
    private UserService userService;

    @Autowired
    public Environment env;

    @Autowired
    public BusConfig busConfig;

    @RequestMapping(value = PATH_PREFIX + "add", method = {RequestMethod.GET,RequestMethod.POST})
    public RspVo add(AddUserEo eo) {
        RspVo<Void> rsp = userService.addUser(eo);
        return rsp;
    }

    @RequestMapping(value = PATH_PREFIX + "queryById", method = {RequestMethod.GET,RequestMethod.POST})
    public RspVo queryById(QueryUserEo eo) {
        RspVo<UserVo> rsp = userService.queryById(eo);
        return rsp;
    }

    @RequestMapping(value = PATH_PREFIX + "edit", method = {RequestMethod.GET,RequestMethod.POST})
    public RspVo edit(EditUserEo eo) {
        RspVo<Void> rsp = userService.editUser(eo);
        return rsp;
    }

    @RequestMapping(value = PATH_PREFIX + "pageQuery", method = {RequestMethod.GET,RequestMethod.POST})
    public RspListVo pageQuery(QueryUserEo eo) {
        RspListVo<UserVo> rsp = userService.pageQuery(eo);
        return rsp;
    }

}
