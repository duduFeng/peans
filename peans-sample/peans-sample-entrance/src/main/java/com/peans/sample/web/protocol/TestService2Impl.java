package com.peans.sample.web.protocol;

import com.peans.core.common.utils.JsonUtils;
import com.peans.core.protocol.MapBeanEo;
import org.springframework.stereotype.Component;

@Component
public class TestService2Impl {

    public TestVo bb1(MapBeanEo eo) {
        System.out.println("cc1");
        System.out.println(JsonUtils.beanToJson(eo));
        System.out.println(eo.getStr(TestEoProperties.name));
        TestVo testVo = new TestVo();
        testVo.setName("bb");
        return testVo;
    }

    public TestVo bb2(MapBeanEo eo) {
        System.out.println("cc2");
        System.out.println(JsonUtils.beanToJson(eo));
        TestVo testVo = new TestVo();
        testVo.setName("bbb2");
        return testVo;
    }
}
