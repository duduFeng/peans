package com.peans.sample.web.protocol;

import org.springframework.stereotype.Component;

@Component
public class TestServiceImpl {

    public TestVo cc1(TestEo eo) {
        System.out.println("cc1");
        TestVo testVo = new TestVo();
        testVo.setName("sss");
        return testVo;
    }

    public TestVo cc2(TestEo eo) {
        System.out.println("cc2");
        TestVo testVo = new TestVo();
        testVo.setName("sss2");
        return testVo;
    }
}
