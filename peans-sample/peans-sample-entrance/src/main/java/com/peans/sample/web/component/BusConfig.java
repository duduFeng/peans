package com.peans.sample.web.component;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by nickfeng on 2017/5/24.
 */
@Component
public class BusConfig {

    @Value("${bus.sample.name}")
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
