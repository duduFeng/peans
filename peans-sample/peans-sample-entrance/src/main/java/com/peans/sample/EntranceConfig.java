package com.peans.sample;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

@SpringBootApplication
//@ImportResource("classpath:dubbo-comsumer.xml")
//@PropertySource("classpath*:busConfig.properties")
@PropertySource({"classpath:busConfig.properties"})
public class EntranceConfig extends SpringBootServletInitializer {

	private static final Log logger = LogFactory.getLog(EntranceConfig.class);

	@Autowired
	public Environment env;

	//@Autowired
	//private DataSourceProperties properties;
	
	/**
	@Bean
	@ConfigurationProperties(prefix = DataSourceProperties.PREFIX)
	public DataSource druidDataSource(){
		DruidDataSource dataSource = new DruidDataSource();
		dataSource.setDriverClassName(properties.getDriverClassName());
		dataSource.setUrl(properties.getUrl());
		dataSource.setUsername(properties.getUsername());
		dataSource.setPassword(properties.getPassword());
		dataSource.setMinIdle(1);
		try {
			dataSource.setFilters("stat");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return dataSource;
	}
	**/

	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		application.web(true);
		//application.bannerMode(Banner.Mode.CONSOLE);
		application.bannerMode(Banner.Mode.OFF);
		return application.sources(EntranceConfig.class);
	}


	/*@Bean
	public MyRequestMappingHandlerMapping getMyRequestMappingHandlerMapping() {
		return new MyRequestMappingHandlerMapping();
	}*/
	/*@Bean
	public MyRequestMappingHandlerMapping2 xxx() {
		MyRequestMappingHandlerMapping2 mapping = new MyRequestMappingHandlerMapping2();
		return mapping;
	}
*/

	public static void main(String[] args) throws Exception {
		SpringApplication app = new SpringApplication(EntranceConfig.class);
		//app.setBannerMode(Banner.Mode.CONSOLE);
		//app.setWebEnvironment(false);
		app.run(args);
	}

}
