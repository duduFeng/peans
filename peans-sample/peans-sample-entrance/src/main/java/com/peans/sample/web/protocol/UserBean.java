package com.peans.sample.web.protocol;

import com.peans.core.boot.auth.UserSessionBean;

public class UserBean implements UserSessionBean {

    private String userId;

    private String name;

    private Integer age;

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @Override
    public String getUserId() {
        return this.userId;
    }
}
