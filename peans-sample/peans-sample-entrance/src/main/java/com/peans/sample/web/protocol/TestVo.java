package com.peans.sample.web.protocol;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.peans.core.common.utils.ReflectUtils;
import com.peans.core.protocol.AbstractVo;
import com.peans.core.protocol.AttributeBean;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;

public class TestVo extends AbstractVo {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static void main(String[] args) {
        /*String path = "apiConfig.xml";
        PeansApiXmpParser parser = new PeansApiXmpParser(path);
        System.out.println(JSONObject.toJSONString(parser.getApisCfgBean()));*/

    }

}
