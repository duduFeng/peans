package com.peans.sample.web.controller;

import com.peans.core.boot.auth.AuthExclude;
import com.peans.core.boot.auth.TokerAuthExecutor;
import com.peans.sample.web.protocol.UserBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
public class EntranceController {


    @Autowired
    private TokerAuthExecutor tokerAuthExecutor;

    @AuthExclude
    @RequestMapping(value = "/getUser")
    public UserBean getUser(HttpServletRequest request) {
        return tokerAuthExecutor.getCurrentUser(UserBean.class,request);
    }

    @AuthExclude
    @RequestMapping(value = "/signup")
    public String singup() {
        System.out.println("");
        UserBean bean = new UserBean();
        bean.setAge(18);
        bean.setName("hehe");
        bean.setUserId("222222");
        return tokerAuthExecutor.signUp(bean);
    }

    @AuthExclude
    @RequestMapping(value = "/signOut")
    public String signOut(HttpServletRequest request) {
        UserBean bean = new UserBean();
        bean.setAge(18);
        bean.setName("hehe");
        bean.setUserId("11");
        tokerAuthExecutor.signOut(request);
        return "OK";
    }


}