package com.peans.sample.web.protocol;

import com.peans.core.common.utils.mapBean.BeanProperty;
import com.peans.core.common.utils.mapBean.MapBean;

import java.util.Date;

/**
 * Created by res-miaofeng on 2016/3/2.
 */
public enum TestEoProperties implements BeanProperty {
    name(String.class),
    age(Integer.class);

    private Class<?> classType;

    private TestEoProperties(Class<?> classType){
        this.classType = classType;
    }

    @Override
    public String getName() {
        return name();
    }

    @Override
    public Class<?> getClassType() {
        return classType;
    }

    public static void main(String[] args) {
        Class type = TestEoProperties.class;
        Object[] objects = type.getEnumConstants();
        System.out.println(objects);

    }


}
