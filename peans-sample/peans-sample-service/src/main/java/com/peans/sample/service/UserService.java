package com.peans.sample.service;

import com.peans.core.protocol.RspListVo;
import com.peans.core.protocol.RspVo;
import com.peans.sample.protocol.AddUserEo;
import com.peans.sample.protocol.EditUserEo;
import com.peans.sample.protocol.QueryUserEo;
import com.peans.sample.protocol.UserVo;

/**
 * Created by nickfeng on 2017/5/25.
 */
public interface UserService {

    RspVo<Void> addUser(AddUserEo eo);

    RspVo<Void> editUser(EditUserEo eo);

    RspVo<UserVo> queryById(QueryUserEo eo);

    RspListVo<UserVo> pageQuery(QueryUserEo eo);
}
