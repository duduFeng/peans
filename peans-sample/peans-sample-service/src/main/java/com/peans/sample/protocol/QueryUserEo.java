package com.peans.sample.protocol;

import com.peans.core.protocol.PageQueryEo;

/**
 * Created by nickfeng on 2017/5/25.
 */
public class QueryUserEo extends PageQueryEo {

    private Integer id;

    private Long name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Long getName() {
        return name;
    }

    public void setName(Long name) {
        this.name = name;
    }
}
