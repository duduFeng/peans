package com.peans.sample.protocol;

/**
 * Created by nickfeng on 2017/5/25.
 */
public class UserVo {

    private String name;

    private Integer age;

    private Integer id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
