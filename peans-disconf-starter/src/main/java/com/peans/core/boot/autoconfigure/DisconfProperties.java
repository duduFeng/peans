package com.peans.core.boot.autoconfigure;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Created by admin on 2016/9/20.
 */
@ConfigurationProperties(prefix = "peans.disconf")
public class DisconfProperties {

    private String scanPackage;

    public String getScanPackage() {
        return scanPackage;
    }

    public void setScanPackage(String scanPackage) {
        this.scanPackage = scanPackage;
    }
}
