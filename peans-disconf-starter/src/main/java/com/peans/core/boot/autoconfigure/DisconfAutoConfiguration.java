package com.peans.core.boot.autoconfigure;

import com.baidu.disconf.client.DisconfMgrBean;
import com.baidu.disconf.client.DisconfMgrBeanSecond;
import com.baidu.disconf.client.addons.properties.ReloadablePropertiesFactoryBean;
import com.baidu.disconf.client.addons.properties.ReloadingPropertyPlaceholderConfigurer;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.util.List;
import java.util.Properties;

/**
 * Created by admin on 2016/9/20.
 */
@Configuration
//@ImportResource({"classpath*:disconf.xml"})//引入disconf
@EnableConfigurationProperties(DisconfProperties.class)
public class DisconfAutoConfiguration {

    @Autowired
    private DisconfProperties properties;

  /*  public DisconfAutoConfiguration() {

    }*/

    @Bean(destroyMethod = "destroy")
    public DisconfMgrBean getDisconfMgrBean() {
        DisconfMgrBean bean = new DisconfMgrBean();
        bean.setScanPackage("com.peans.sample.web");
        //bean.setScanPackage(properties.getScanPackage());
        return bean;
    }

    @Bean(destroyMethod = "destroy",initMethod = "init")
    public DisconfMgrBeanSecond getDisconfMgrBeanSecond() {
        DisconfMgrBeanSecond bean = new DisconfMgrBeanSecond();
        return bean;
    }

    @Bean
    public ReloadablePropertiesFactoryBean getReloadablePropertiesFactoryBean() {
        ReloadablePropertiesFactoryBean bean = new ReloadablePropertiesFactoryBean();
        List<String> fileNames = Lists.newArrayList();
        //fileNames.add("classpath*:busConfig.properties");
        fileNames.add("classpath*:auto.properties");
        bean.setLocations(fileNames);
        return bean;
    }

    @Bean
    public ReloadingPropertyPlaceholderConfigurer getReloadingPropertyPlaceholderConfigurer() throws IOException {
        ReloadingPropertyPlaceholderConfigurer bean = new ReloadingPropertyPlaceholderConfigurer();
        bean.setIgnoreResourceNotFound(true);
        bean.setIgnoreUnresolvablePlaceholders(true);
        Properties properties = getReloadablePropertiesFactoryBean().getObject();
        bean.setProperties(properties);
        return bean;
    }

}
