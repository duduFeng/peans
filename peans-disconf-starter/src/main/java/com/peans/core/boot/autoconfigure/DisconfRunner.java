package com.peans.core.boot.autoconfigure;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;

/**
 * Created by admin on 2016/9/20.
 */
public class DisconfRunner implements CommandLineRunner {

    protected final Logger logger = LoggerFactory.getLogger(DisconfRunner.class);

    @Override
    public void run(String... args) throws Exception {
        System.out.println("disconf自动配置启动成功.......");
        logger.info("disconf自动配置启动成功.......");
    }
}
