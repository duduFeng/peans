package com.peans.core.boot.api.parser;

import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.condition.PatternsRequestCondition;
import org.springframework.web.servlet.mvc.condition.RequestMethodsRequestCondition;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import java.lang.reflect.Method;
import java.util.List;

@RequestMapping
public class ApiRequestMapping extends RequestMappingHandlerMapping {

    private ApiConfigParser parser;

    public ApiRequestMapping(ApiConfigParser parser) {
        super();
        this.parser =  parser;
    }

    @Override
    public void afterPropertiesSet() {
        super.afterPropertiesSet();
        ApiEntranceController aa = new ApiEntranceController();
        //System.out.println("###########");
        detectHandlerMethods(aa);
    }

    @Override
    protected boolean isHandler(Class<?> beanType) {
        return false;
    }

/*    @Override
    protected RequestMappingInfo getMappingForMethod(Method method, Class<?> handlerType) {
        return super.getMappingForMethod(method,handlerType);
    }*/

    @Override
    @Deprecated
    protected void registerHandlerMethod(Object handler, Method method,
                                         RequestMappingInfo mapping) {
        if (mapping == null) {
            return;
        }
        String[] patterns = getPatterns(handler, mapping);

        if (!ObjectUtils.isEmpty(patterns)) {
            super.registerHandlerMethod(handler, method,
                    withNewPatterns(mapping, patterns));
        }
    }

    private String[] getPatterns(Object handler, RequestMappingInfo mapping) {
        List<String> paths = parser.getPaths();
        return paths.toArray(new String[paths.size()]);
        //return new String[]{"ccgg","cc1"};
    }


    private RequestMappingInfo withNewPatterns(RequestMappingInfo mapping,
                                               String[] patternStrings) {
        PatternsRequestCondition patterns = new PatternsRequestCondition(patternStrings,
                null, null, useSuffixPatternMatch(), useTrailingSlashMatch(), null);
        return new RequestMappingInfo(patterns, new RequestMethodsRequestCondition(RequestMethod.GET),
                mapping.getParamsCondition(), mapping.getHeadersCondition(),
                mapping.getConsumesCondition(), mapping.getProducesCondition(),
                mapping.getCustomCondition());
    }

}
