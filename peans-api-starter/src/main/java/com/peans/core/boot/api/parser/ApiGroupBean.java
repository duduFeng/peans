package com.peans.core.boot.api.parser;

import java.util.List;

public class ApiGroupBean {

    private String name;

    private String description;

    private String executor;

    private String basePath;

    private List<ApiCfgBean> apis;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public String getExecutor() {
        return executor;
    }

    public void setExecutor(String executor) {
        this.executor = executor;
    }

    public String getBasePath() {
        return basePath;
    }

    public void setBasePath(String basePath) {
        this.basePath = basePath;
    }

    public List<ApiCfgBean> getApis() {
        return apis;
    }

    public void setApis(List<ApiCfgBean> apis) {
        this.apis = apis;
    }
}
