package com.peans.core.boot.autoconfigure;

import com.peans.core.boot.api.parser.ApiConfigParser;
import com.peans.core.boot.api.parser.ApiRequestMapping;
import com.peans.core.boot.api.parser.PeansApiXmpParser;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(value = {"com.peans.core.boot.api.parser"})
@EnableConfigurationProperties(PeansApiProperties.class)
public class PeansApiAutoConfiguration {

	private final ApplicationContext applicationContext;

	private final PeansApiProperties properties;

	public PeansApiAutoConfiguration(ApplicationContext applicationContext,
									 PeansApiProperties properties) {
		this.applicationContext = applicationContext;
		this.properties = properties;
	}

	@Bean
	@ConditionalOnMissingBean(name = "apiConfigParser")
	public ApiConfigParser getApiConfigParser() {
		String path = properties.getApiConfigPath();
		PeansApiXmpParser parser = new PeansApiXmpParser(path);
		return parser;
	}

    @Bean
	@ConditionalOnMissingBean(name = "apiRequestMapping")
	public ApiRequestMapping getApiRequestMapping(ApiConfigParser apiConfigParser) {
		return new ApiRequestMapping(apiConfigParser);
}



}
