package com.peans.core.boot.api.parser;

import com.peans.core.protocol.AbstractVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
public class ApiEntranceController {

    @Autowired
    private ApiDispatcher apiDispatcher;

    @RequestMapping
    @ResponseBody
    public AbstractVo invoke(HttpServletRequest request) {
        return apiDispatcher.dispatch(request);
    }

}
