package com.peans.core.boot.api.parser;

import com.google.common.collect.Maps;
import com.peans.core.common.utils.mapBean.BeanProperty;

import javax.xml.crypto.Data;
import java.util.Map;

public class MapBeanCfg implements BeanProperty {

    private String key;

    private String description;

    private String type;

    private static Map<String,Class<?>> typeMaps = Maps.newHashMap();

    static{
        typeMaps.put("String",String.class);
        typeMaps.put("Integer",Integer.class);
        typeMaps.put("Long",Long.class);
        typeMaps.put("Date",Data.class);
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String getName() {
        return key;
    }

    @Override
    public Class<?> getClassType() {
        return null;
    }
}
