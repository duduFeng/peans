package com.peans.core.boot.api.parser;

import java.util.List;
import java.util.Map;

public interface ApiConfigParser {

    //public ApisCfgBean parse();

    public ApiCfgBean getCfgByPath(String path);

    public List<String> getPaths();

    public ApisCfgBean getApisCfgBean();

    public Map<String, ApiCfgBean> getConfigMap();

}
