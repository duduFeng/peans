package com.peans.core.boot.api.parser;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.peans.core.common.utils.ReflectUtils;
import org.apache.commons.lang3.StringUtils;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.springframework.util.CollectionUtils;

import java.io.InputStream;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class PeansApiXmpParser implements ApiConfigParser{

    private String filePath;

    private Map<String, ApiCfgBean> configMap = Maps.newHashMap();

    private ApisCfgBean apisCfgBean = new ApisCfgBean();

    public PeansApiXmpParser(String filePath) {
        this.filePath = filePath;
        ApisCfgBean cfgBean = parse();
        Map<String, ApiCfgBean> map = getApiCfgMap(cfgBean);
        this.configMap = map;
        this.apisCfgBean = cfgBean;
    }

    private Map<String, ApiCfgBean> getApiCfgMap(ApisCfgBean bean) {
        Map<String, ApiCfgBean> map = Maps.newHashMap();
        if (bean != null) {
            List<ApiGroupBean> apiGroupBeans = bean.getGroupBeans();
            if (!CollectionUtils.isEmpty(apiGroupBeans)) {
                for (ApiGroupBean groupBean:apiGroupBeans) {
                    if (!CollectionUtils.isEmpty(groupBean.getApis())) {
                        for (ApiCfgBean cfgBean : groupBean.getApis()) {
                            map.put(cfgBean.getPath(),cfgBean);
                        }
                    }
                }
            }
        }
        return map;
    }

    private ApisCfgBean parse() {
        ApisCfgBean cfgBean = null;
        SAXReader reader = new SAXReader();
        try {
            InputStream in = Thread.currentThread().getContextClassLoader().getResourceAsStream(filePath);
            if (in == null) {
                throw new RuntimeException("找不到Api配置文件");
            }
            Document document = reader.read(in);
            Element rootElement = document.getRootElement();
            cfgBean = new ApisCfgBean();
            cfgBean.setModelBasePackage(rootElement.attributeValue("modelBasePackage"));
            Map<String, List<MapBeanCfg>> mapBeans = parseMapBeans(cfgBean,rootElement);
            cfgBean.setMapBeans(mapBeans);
            List<ApiGroupBean> groupBeans = parseApiGroup(cfgBean, rootElement);
            cfgBean.setGroupBeans(groupBeans);
        } catch (DocumentException e) {
            e.printStackTrace();
            throw new RuntimeException("文件解析错误",e);
        }
        return cfgBean;
    }

    private Map<String,List<MapBeanCfg>> parseMapBeans(ApisCfgBean cfgBean, Element rootElement) {
        Map<String,List<MapBeanCfg>> result = Maps.newHashMap();
        Iterator it = rootElement.elements("apiGroup").iterator();
        List<MapBeanCfg> list = Lists.newArrayList();
        while (it.hasNext()) {
            Element e = (Element) it.next();
            Iterator sit = e.elements("property").iterator();
            while (sit.hasNext()) {
                Element ee = (Element) it.next();
                MapBeanCfg mc = new MapBeanCfg();
                mc.setDescription(ee.attributeValue("description"));
                mc.setType(ee.attributeValue("type"));
                mc.setKey(ee.attributeValue("key"));
                list.add(mc);
            }
            result.put(e.attributeValue("id"),list);
        }
        return result;
    }

    private List<ApiGroupBean> parseApiGroup(ApisCfgBean cfgBean, Element rootElement) {
        List<ApiGroupBean> groupBeans = Lists.newArrayList();
        Iterator it = rootElement.elements("apiGroup").iterator();
        while (it.hasNext()) {
            Element e = (Element) it.next();
            ApiGroupBean bean = new ApiGroupBean();
            bean.setName(e.attributeValue("name"));
            bean.setDescription(e.attributeValue("description"));
            bean.setBasePath(e.attributeValue("basePath"));
            bean.setExecutor(e.attributeValue("executor"));
            List<ApiCfgBean> apiCfgBeans = Lists.newArrayList();
            Iterator itt = e.elementIterator();
            while (itt.hasNext()) {
                Element ee = (Element) itt.next();
                ApiCfgBean aBean = new ApiCfgBean();
                if (StringUtils.isNotBlank(bean.getBasePath())) {
                    aBean.setPath(bean.getBasePath()+ee.attributeValue("path"));
                } else {
                    aBean.setPath(ee.attributeValue("path"));
                }
                if (StringUtils.isNotBlank(cfgBean.getModelBasePackage())) {
                    aBean.setEoClass(cfgBean.getModelBasePackage()+"."+ee.attributeValue("eoClass"));
                } else {
                    aBean.setEoClass(ee.attributeValue("eoClass"));
                }
                if (StringUtils.isNotBlank(cfgBean.getModelBasePackage())) {
                    aBean.setVoClass(cfgBean.getModelBasePackage()+"."+ee.attributeValue("voClass"));
                } else {
                    aBean.setVoClass(ee.attributeValue("voClass"));
                }
                try {
                    aBean.setEoAttributes(ReflectUtils.getAttributeBean(Class.forName(aBean.getEoClass())));
                    aBean.setVoAttributes(ReflectUtils.getAttributeBean(Class.forName(aBean.getVoClass())));
                } catch (ClassNotFoundException e1) {
                    //e1.printStackTrace();
                }
                if (StringUtils.isNotBlank(ee.attributeValue("eoMapId"))) {

                }
                aBean.setExecutor(bean.getExecutor());
                //aBean.setEoMapProperty(ee.attributeValue("eoMapProperty"));
                aBean.setMethod(ee.attributeValue("method"));
                aBean.setDescription(ee.attributeValue("description"));
                apiCfgBeans.add(aBean);
            }
            bean.setApis(apiCfgBeans);
            groupBeans.add(bean);
        }
        return groupBeans;
    }

    public ApiCfgBean getCfgByPath(String path) {
        return configMap.get(path);
    }

    public List<String> getPaths() {
        List<String> result = Lists.newArrayList();
        for (Map.Entry<String,ApiCfgBean> me : configMap.entrySet()) {
            result.add(me.getKey());
        }
        return result;
    }

    public ApisCfgBean getApisCfgBean() {
        return apisCfgBean;
    }

    public Map<String, ApiCfgBean> getConfigMap() {
        return configMap;
    }

/*public static void main(String[] args) {
        String path = "apiConfig.xml";
        PeansApiXmpParser parser = new PeansApiXmpParser(path);
        ApisCfgBean cfgBean = parser.parse();
        System.out.println(cfgBean);
    }*/

}
