package com.peans.core.boot.api.parser;

import com.peans.core.protocol.AttributeBean;

import java.util.List;

public class ApiCfgBean {

    private String path;

    private String eoClass;

    private String voClass;

    private String executor;

    private String method;

    private String description;

    private String eoMapId;

    private String voMapId;

    //private String eoMapProperty;

    private List<AttributeBean> eoAttributes;

    private List<AttributeBean> voAttributes;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getEoClass() {
        return eoClass;
    }

    public void setEoClass(String eoClass) {
        this.eoClass = eoClass;
    }

    public String getVoClass() {
        return voClass;
    }

    public void setVoClass(String voClass) {
        this.voClass = voClass;
    }

    public String getExecutor() {
        return executor;
    }

    public void setExecutor(String executor) {
        this.executor = executor;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<AttributeBean> getEoAttributes() {
        return eoAttributes;
    }

    public void setEoAttributes(List<AttributeBean> eoAttributes) {
        this.eoAttributes = eoAttributes;
    }

    public List<AttributeBean> getVoAttributes() {
        return voAttributes;
    }

    public void setVoAttributes(List<AttributeBean> voAttributes) {
        this.voAttributes = voAttributes;
    }

    public String getEoMapId() {
        return eoMapId;
    }

    public void setEoMapId(String eoMapId) {
        this.eoMapId = eoMapId;
    }

    public String getVoMapId() {
        return voMapId;
    }

    public void setVoMapId(String voMapId) {
        this.voMapId = voMapId;
    }

}
