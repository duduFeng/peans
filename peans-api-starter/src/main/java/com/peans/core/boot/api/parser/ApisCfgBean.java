package com.peans.core.boot.api.parser;

import java.util.List;
import java.util.Map;

public class ApisCfgBean {

    private List<ApiGroupBean> groupBeans;

    private String modelBasePackage;

    private String executorBasePackage;

    private Map<String,List<MapBeanCfg>> mapBeans;

    public List<ApiGroupBean> getGroupBeans() {
        return groupBeans;
    }

    public void setGroupBeans(List<ApiGroupBean> groupBeans) {
        this.groupBeans = groupBeans;
    }

    public String getModelBasePackage() {
        return modelBasePackage;
    }

    public void setModelBasePackage(String modelBasePackage) {
        this.modelBasePackage = modelBasePackage;
    }

    public String getExecutorBasePackage() {
        return executorBasePackage;
    }

    public void setExecutorBasePackage(String executorBasePackage) {
        this.executorBasePackage = executorBasePackage;
    }

    public Map<String, List<MapBeanCfg>> getMapBeans() {
        return mapBeans;
    }

    public void setMapBeans(Map<String, List<MapBeanCfg>> mapBeans) {
        this.mapBeans = mapBeans;
    }
}
