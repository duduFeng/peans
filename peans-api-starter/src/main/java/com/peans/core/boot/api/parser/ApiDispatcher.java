package com.peans.core.boot.api.parser;

import com.alibaba.fastjson.JSONObject;
import com.peans.core.common.utils.ServletUtils;
import com.peans.core.common.utils.mapBean.BeanProperty;
import com.peans.core.common.utils.mapBean.MapBean;
import com.peans.core.common.utils.mapBean.MapBeanUtils;
import com.peans.core.protocol.AbstractVo;
import com.peans.core.protocol.MapBeanEo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import org.springframework.util.ReflectionUtils;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;

@Component
public class ApiDispatcher implements ApplicationContextAware {

    private ApplicationContext applicationContext;

    private ApiConfigParser apiConfigParser;

    public ApiDispatcher(ApiConfigParser parser) {
        apiConfigParser = parser;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    public AbstractVo dispatch(HttpServletRequest request) {
        String url = request.getRequestURI();
        ApiCfgBean bean = apiConfigParser.getCfgByPath(url);
        ApisCfgBean cfg = apiConfigParser.getApisCfgBean();
        if (bean != null) {
            try {
                Class<?> c = Class.forName(bean.getExecutor());
                Object obj = applicationContext.getBean(c);
                Map<String, String> paramMap = ServletUtils.getParamsMapFromRequest(request);
                String str = JSONObject.toJSONString(paramMap);
                Object eo = null;
                if (StringUtils.isBlank(bean.getEoClass()) && cfg.getMapBeans().containsKey(bean.getEoMapId())) {
                    List<MapBeanCfg> cfgist = cfg.getMapBeans().get(bean.getEoMapId());
                    BeanProperty[] array = cfgist.toArray(new BeanProperty[cfgist.size()]);
                    MapBean bb = MapBeanUtils.parseJson(str,array);
                    MapBeanEo me = new MapBeanEo();
                    me.putAll(bb);
                    eo = me;
                } else {
                    Class<?> mc = Class.forName(bean.getEoClass());
                    if (mc.isEnum() && (BeanProperty.class.isAssignableFrom(mc))) {
                        BeanProperty[] array = (BeanProperty[]) mc.getEnumConstants();
                        MapBean bb = MapBeanUtils.parseJson(str,array);
                        MapBeanEo me = new MapBeanEo();
                        me.putAll(bb);
                        eo = me;
                    } else {
                        eo = JSONObject.parseObject(str,mc);
                    }
                }
                // TODO: 2017/9/22  检验eo vo
                Method method = ReflectionUtils.findMethod(c,bean.getMethod(),eo.getClass());
                if (method == null) {
                    throw new RuntimeException("找不到方法名");
                }
                Object returnObj = method.invoke(obj,eo);
                return (AbstractVo)returnObj;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
