package com.peans.core.boot.autoconfigure;

import com.peans.core.boot.api.parser.ApiConfigParser;
import com.peans.core.boot.api.parser.ApiRequestMapping;
import com.peans.core.boot.api.parser.PeansApiXmpParser;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@ConfigurationProperties(prefix = "peans.api")
public class PeansApiProperties {

	private String apiConfigPath;


	public String getApiConfigPath() {
		if (apiConfigPath == null) {
			return "apiConfig.xml";
		}
		return apiConfigPath;
	}

	public void setApiConfigPath(String apiConfigPath) {
		this.apiConfigPath = apiConfigPath;
	}
}
