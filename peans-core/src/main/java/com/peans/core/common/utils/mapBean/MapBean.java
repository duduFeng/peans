package com.peans.core.common.utils.mapBean;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by res-miaofeng on 2016/3/2.
 */
@JsonSerialize(using = MapBeanJsonSerializer.class)
public class MapBean implements Map<BeanProperty,Object> {

    private Map<BeanProperty,Object> content;

    public MapBean(){
        this.content = new HashMap<>(20);
    }

    public void add(BeanProperty p,Object value){
        if (p != null){
            if (value == null){
                content.put(p,value);
            }else {
                if (value.getClass().isAssignableFrom(p.getClassType())){
                    content.put(p,value);
                }
            }
        }
    }

    public Object get(BeanProperty p){
        content.isEmpty();
        return content.get(p);
    }

    @Override
    public int size() {
        return content.size();
    }

    @Override
    public boolean isEmpty() {
        return content.isEmpty();
    }

    @Override
    public boolean containsKey(Object key) {
        return content.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value) {
        return content.containsValue(value);
    }

    @Override
    public Object get(Object key) {
        return content.get(key);
    }

    @Override
    public Object put(BeanProperty key, Object value) {
        return content.put(key,value);
    }

    @Override
    public Object remove(Object key) {
        return content.remove(key);
    }

    @Override
    public void putAll(Map<? extends BeanProperty, ?> m) {
        content.putAll(m);
    }

    @Override
    public void clear() {
        content.clear();
    }

    @Override
    public Set<BeanProperty> keySet() {
        return content.keySet();
    }

    @Override
    public Collection<Object> values() {
        return content.values();
    }

    @Override
    public Set<Entry<BeanProperty, Object>> entrySet() {
        return content.entrySet();
    }

    public Map<BeanProperty, Object> getContent() {
        return content;
    }



    /**
     * Get attribute of mysql type: varchar, char, enum, set, text, tinytext, mediumtext, longtext
     */
    public String getStr(BeanProperty p) {
        return (String)content.get(p);
    }

    /**
     * Get attribute of mysql type: int, integer, tinyint(n) n > 1, smallint, mediumint
     */
    public Integer getInt(BeanProperty p) {
        return (Integer)content.get(p);
    }

    /**
     * Get attribute of mysql type: bigint, unsign int
     */
    public Long getLong(BeanProperty p) {
        return (Long)content.get(p);
    }

    /**
     * Get attribute of mysql type: unsigned bigint
     */
    public java.math.BigInteger getBigInteger(BeanProperty p) {
        return (java.math.BigInteger)content.get(p);
    }

    /**
     * Get attribute of mysql type: date, year
     */
    public java.util.Date getDate(BeanProperty p) {
        return (java.util.Date)content.get(p);
    }

    /**
     * Get attribute of mysql type: time
     */
    public java.sql.Time getTime(BeanProperty p) {
        return (java.sql.Time)content.get(p);
    }

    /**
     * Get attribute of mysql type: timestamp, datetime
     */
    public java.sql.Timestamp getTimestamp(BeanProperty p) {
        return (java.sql.Timestamp)content.get(p);
    }

    /**
     * Get attribute of mysql type: real, double
     */
    public Double getDouble(BeanProperty p) {
        return (Double)content.get(p);
    }

    /**
     * Get attribute of mysql type: float
     */
    public Float getFloat(BeanProperty p) {
        return (Float)content.get(p);
    }

    /**
     * Get attribute of mysql type: bit, tinyint(1)
     */
    public Boolean getBoolean(BeanProperty p) {
        return (Boolean)content.get(p);
    }

    /**
     * Get attribute of mysql type: decimal, numeric
     */
    public java.math.BigDecimal getBigDecimal(BeanProperty p) {
        return (java.math.BigDecimal)content.get(p);
    }

    /**
     * Get attribute of mysql type: binary, varbinary, tinyblob, blob, mediumblob, longblob
     */
    public byte[] getBytes(BeanProperty p) {
        return (byte[])content.get(p);
    }

    /**
     * Get attribute of any type that extends from Number
     */
    public Number getNumber(BeanProperty p) {
        return (Number)content.get(p);
    }
}
