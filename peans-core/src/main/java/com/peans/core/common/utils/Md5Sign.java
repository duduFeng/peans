package com.peans.core.common.utils;

import com.google.common.collect.Lists;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import java.io.UnsupportedEncodingException;
import java.security.SignatureException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * md5签名
 * @author nickfeng
 *
 */
public final class Md5Sign {
	
	private static final Logger logger = LoggerFactory.getLogger(Md5Sign.class);

	private final static String inputCharset = "utf-8";
	
	private Md5Sign(){
		
	}
	
	public static String buildSign(Map<String, String> params,String secretKey){
		Assert.hasText(secretKey);
        // 过滤参数
        Map<String, String> sParaNew = paraFilter(params,new String[]{"sign","sign_type"});
        // 获取待签名字符串
        String preSignStr = StringKit.createLinkString(sParaNew);
        //加入key
    	String t = preSignStr + secretKey;
    	String result = DigestUtils.md5Hex(getContentBytes(t, inputCharset));
		return result;
	}
	


	private static  Map<String, String> paraFilter(Map<String, String> sArray,String[] excludeParams) {
        Map<String, String> result = new HashMap<String, String>();
        if (sArray == null || sArray.size() <= 0) {
            return result;
        }
        List<String> strs = Lists.newArrayList(excludeParams);
        for (String key : sArray.keySet()) {
            String value = sArray.get(key);
            if (StringUtils.isNotEmpty(value)&&!strs.contains(key)) {
            	result.put(key, value);
			}
        }
        return result;
    }


    /**
     * @param content
     * @param charset
     * @return
     * @throws SignatureException
     * @throws UnsupportedEncodingException 
     */
    public static byte[] getContentBytes(String content, String charset) {
        if (charset == null || "".equals(charset)) {
            return content.getBytes();
        }
        try {
            return content.getBytes(charset);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("MD5签名过程中出现错误,指定的编码集不对,您目前指定的编码集是:" + charset);
        }
    }
	
    
    
    
	
}
