package com.peans.core.common.utils;

import java.math.BigDecimal;

public enum NumberUtils {
	
	;
	public static double convertOfScacle(int sacle,double d){
		return new BigDecimal(d).setScale(sacle, BigDecimal.ROUND_HALF_UP).doubleValue();
	}
}
