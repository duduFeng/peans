package com.peans.core.common.utils;

public class EncryptionUtil {
	public static String encryption(String account){
		String reStr=null;
		int index = account.indexOf("@");
		if(index>2){//邮箱
			reStr=account.replaceFirst(account.substring(1, index-1), "****");
		}else if(index>-1){//手机
			return account;
		}else{
			reStr=account.replaceFirst(account.substring(3, 7), "****");
		}
		return reStr;
	}
	public static void main(String[] args) {
		System.out.println(encryption("13266667777"));
		System.out.println(encryption("ab@qq.com"));
		System.out.println(encryption("abc@qq.com"));
	}
}
