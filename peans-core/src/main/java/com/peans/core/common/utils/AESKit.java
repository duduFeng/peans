package com.peans.core.common.utils;

import com.google.common.base.Joiner;
import com.google.common.base.Throwables;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public enum  AESKit {
    ;
    // 加密
    public static String encrypt(String sSrc, String sKey){
        if (StringUtils.isBlank(sSrc)) {
            throw new RuntimeException("加密串不能为空");
        }
        if (StringUtils.isBlank(sKey)) {
            throw new RuntimeException("秘钥不能为空");
        }
        if (sKey.length() != 16) {
            throw new RuntimeException("秘钥长度只能为16位");
        }
        try {
            byte[] raw = sKey.getBytes("utf-8");
            SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
            byte[] encrypted = cipher.doFinal(sSrc.getBytes("utf-8"));
            return new Base64().encodeToString(encrypted);
        } catch (Exception e) {
            e.printStackTrace();
            Throwables.propagateIfPossible(e);
        }
        return null;
    }

    // 解密
    public static String decrypt(String sSrc, String sKey){
        if (StringUtils.isBlank(sSrc)) {
            throw new RuntimeException("解密串不能为空");
        }
        if (StringUtils.isBlank(sKey)) {
            throw new RuntimeException("秘钥不能为空");
        }
        if (sKey.length() != 16) {
            throw new RuntimeException("秘钥长度只能为16位");
        }
        try {
            byte[] raw = sKey.getBytes("utf-8");
            SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.DECRYPT_MODE, skeySpec);
            byte[] encrypted1 = new Base64().decode(sSrc);//先用base64解密
            byte[] original = cipher.doFinal(encrypted1);
            String originalString = new String(original,"utf-8");
            return originalString;
        } catch (Exception ex) {
            ex.printStackTrace();
            Throwables.propagateIfPossible(ex);
        }
        return null;
    }

    public static void main(String[] args) throws Exception {
        /*
         * 此处使用AES-128-ECB加密模式，key需要为16位。
         */
        String cKey = "1234567890123456";
        // 需要加密的字串
        String cSrc = "www.gowhere.so";
        System.out.println(cSrc);
        // 加密
        String enString = AESKit.encrypt(cSrc, cKey);
        System.out.println("加密后的字串是：" + enString);

        // 解密
        String DeString = AESKit.decrypt(enString, cKey);
        System.out.println("解密后的字串是：" + DeString);
    }

}
