package com.peans.core.common.utils;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializeConfig;
import com.alibaba.fastjson.serializer.SimpleDateFormatSerializer;


public final class JsonUtils {

	private  JsonUtils() {
		
	}
	
	private static SerializeConfig mapping = new SerializeConfig();  
	private static String dateFormat;  
	static {  
	    dateFormat = "yyyy-MM-dd HH:mm:ss";  
	    mapping.put(Date.class, new SimpleDateFormatSerializer(dateFormat));  
	}
	
	public static JSONObject jsonToJSONObject(String json) {
		JSONObject object = JSONObject.parseObject(json);
		return  object;
	}

	public static String mapToJson(Map<String, Object> map) {
		return JSON.toJSONString(map,true);
	}
	
	public static String beanToJson(Object object){
		return JSON.toJSONString(object, mapping);
	}
	
	public static String arrayToJson(List<?> object){
		return JSONArray.toJSONString(object);
	}
	
	public static <T> List<T> jsonToArray(String json,Class<T> clazz){
		return JSONArray.parseArray(json, clazz);
	}
	
	public final static  <T> T jsonToObject (String json,Class<T> clazz){
		return JSON.parseObject(json, clazz);
	}

	public static void sysout(Object objet){
		System.out.println(beanToJson(objet));
	}
}
