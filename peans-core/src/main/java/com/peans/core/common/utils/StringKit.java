package com.peans.core.common.utils;

import com.google.common.collect.Maps;
import org.apache.commons.lang3.StringUtils;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public enum StringKit {
	;
	public static String createLinkString(Map<String, String> params) {
        List<String> keys = new ArrayList<String>(params.keySet());
        Collections.sort(keys);
        String prestr = "";
        for (int i = 0; i < keys.size(); i++) {
            String key = keys.get(i);
            String value = params.get(key);
            if (StringUtils.isNotEmpty(value)) {
            	prestr = prestr + key + "=" + value + "&";
			}
        }
        prestr = prestr.substring(0, prestr.length()-1);
        return prestr;
    }
	
	public static String createLinkStringWithQuotes(Map<String, String> params) {
        List<String> keys = new ArrayList<String>(params.keySet());
        Collections.sort(keys);
        String prestr = "";
        for (int i = 0; i < keys.size(); i++) {
            String key = keys.get(i);
            String value = params.get(key);
            if (StringUtils.isNotEmpty(value)) {
            	prestr = prestr + key + "=\"" + value + "\"&";
			}
        }
        prestr = prestr.substring(0, prestr.length()-1);
        return prestr;
    }

    public static Map<String, String> xmlStrToMap(String result) {
        Map<String, String> resultMap = null;
        if (StringUtils.isNotEmpty(result)) {
            Document doc;
            resultMap = Maps.newHashMap();
            try {
                doc = DocumentHelper.parseText(result);
                Element rootElt = doc.getRootElement(); // 获取根节点
                List<Element> list = rootElt.elements();
                if (!CollectionUtils.isEmpty(list)) {
                    for (Element element : list) {
                        resultMap.put(element.getName(), element.getTextTrim());
                    }
                }
            } catch (DocumentException e) {
                e.printStackTrace();
            }
        }
        return resultMap;
    }
	
}
