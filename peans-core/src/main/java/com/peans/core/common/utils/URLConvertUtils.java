package com.peans.core.common.utils;

import java.util.Map;
import java.util.Set;

/**
 * 包名: com.hongma.homa.utils
 * 描 述: 请求拼接工具类
 * 创建人: Zengjie
 * 创建日期: 2015/8/14
 */

public enum URLConvertUtils {

	;
	
    public static String convertUrl(StringBuffer urlbuf, Map<String, String> params) {
        if (params != null) {
            int n = 0;

            for (Map.Entry<String, String> set : params.entrySet()) { //entrySet获得map所有对象
                if (!urlbuf.toString().contains("?")) {
                    urlbuf.append("?");
                }
                if (n != 0) {
                    urlbuf.append("&");
                }
                urlbuf.append(set.getKey()).append("=")
                        .append(set.getValue());
                n++;
            }
        }
        return urlbuf.toString();
    }

    public static String convertParamField(Set<String> params) {
        StringBuffer buf = new StringBuffer();
        if (params.size()==1){
            buf.append(params.iterator().next());
        }else {
            int n = 0;
            for (String value : params) {
                if (n!=(params.size()-1)){
                    buf.append(value+",");
                }else {
                    buf.append(value);
                }
                n++;
            }
        }
        return buf.toString();
    }


}
