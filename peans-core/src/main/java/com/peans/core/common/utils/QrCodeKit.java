package com.peans.core.common.utils;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.imageio.ImageIO;

public class QrCodeKit {

 /*   public static void main(String[] args) {

        String content = "http://blog.csdn.net/mmm333zzz/article/details/17259513";
        createQRCode(content, "c:\\dev\\tronker1.png", null);
        while(true){
        	
        }
        // createQRCode(content,"d:/tronker1.png", "D://64.png");
    }

    public static byte[] createQRCode(String content, String form, String logo) {
        byte[] result = null;
        OutputStream ups = null;
        try {
            Qrcode qrcodeHandler = new Qrcode();
            qrcodeHandler.setQrcodeErrorCorrect('M');
            qrcodeHandler.setQrcodeEncodeMode('B');
            qrcodeHandler.setQrcodeVersion(7);

            byte[] contentBytes = content.getBytes("utf-8");

            BufferedImage bufferImgage = new BufferedImage(250, 250, BufferedImage.TYPE_INT_RGB);

            Graphics2D graphics2D = bufferImgage.createGraphics();
            graphics2D.setBackground(Color.white);
            graphics2D.clearRect(0, 0, 250, 250);
            graphics2D.setColor(new Color(253, 152, 154));
            int pixoff = 12;
            if (contentBytes.length > 0 && contentBytes.length < 120) {
                boolean[][] matrix = qrcodeHandler.calQrcode(contentBytes);
                for (int i = 0; i < matrix.length; i++) {
                    for (int j = 0; j < matrix.length; j++) {
                        if (matrix[j][i]) {
                            graphics2D.fillRect(j * 5 + pixoff, i * 5 + pixoff, 5, 5);
                        }
                    }
                }
            } else {
                //
            }

            if (logo != null) {
                Image img = ImageIO.read(new File(logo));
                // 实例化一个Image对象。
                graphics2D.drawImage(img, 93, 93, null);
                graphics2D.dispose();
                bufferImgage.flush();
            }
            // // 实例化一个Image对象。
            // graphics2D.drawImage(img, 93, 93, null);
            // graphics2D.dispose();
            // bufferImgage.flush();

            File f = new File(form);
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            ups =  new FileOutputStream(f);
            ImageIO.write(bufferImgage, "png",ups);
            result = output.toByteArray();
            output.close();
            

        } catch (Exception e) {
            e.printStackTrace();
        }finally{
        	if(ups != null){
        		try {
					ups.close();
				} catch (IOException e) {
				}
        	}
        }

        return result;
    }

    public static int createQRCodeTemp(String content, String imgPath, String ccbPath) {
        try {
            Qrcode qrcodeHandler = new Qrcode();
            qrcodeHandler.setQrcodeErrorCorrect('M');
            qrcodeHandler.setQrcodeEncodeMode('B');
            qrcodeHandler.setQrcodeVersion(7);

            // System.out.println(content);
            byte[] contentBytes = content.getBytes("gb2312");
            BufferedImage bufImg = new BufferedImage(140, 140, BufferedImage.TYPE_INT_RGB);
            Graphics2D gs = bufImg.createGraphics();

            gs.setBackground(Color.WHITE);
            gs.clearRect(0, 0, 140, 140);

            // 设定图像颜色 > BLACK
            gs.setColor(Color.BLACK);
            // 设置偏移量 不设置可能导致解析出错
            int pixoff = 2;
            // 输出内容 > 二维码
            if (contentBytes.length > 0 && contentBytes.length < 120) {
                boolean[][] codeOut = qrcodeHandler.calQrcode(contentBytes);
                for (int i = 0; i < codeOut.length; i++) {
                    for (int j = 0; j < codeOut.length; j++) {
                        if (codeOut[j][i]) {
                            gs.fillRect(j * 3 + pixoff, i * 3 + pixoff, 3, 3);
                        }
                    }
                }
            } else {
                System.err.println("QRCode content bytes length = " + contentBytes.length + " not in [ 0,120 ]. ");
                return -1;
            }
            Image img = ImageIO.read(new File(ccbPath));
            // 实例化一个Image对象。
            gs.drawImage(img, 55, 55, null);
            gs.dispose();
            bufImg.flush();

            // 实例化一个Image对象。
            gs.drawImage(img, 55, 55, null);
            gs.dispose();
            bufImg.flush();
            // 生成二维码QRCode图片
            File imgFile = new File(imgPath);
            ImageIO.write(bufImg, "png", imgFile);
        } catch (Exception e) {
            e.printStackTrace();
            return -100;
        }
        return 0;
    }*/
}
