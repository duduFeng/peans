package com.peans.core.common.utils;

import com.peans.core.common.utils.mapBean.BeanProperty;
import com.peans.core.common.utils.mapBean.MapBean;
import com.peans.core.common.utils.mapBean.MapBeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * md5签名
 * @author nickfeng
 *
 */
public final class ServletUtils {

	private static final Logger logger = LoggerFactory.getLogger(ServletUtils.class);


	private ServletUtils(){
		
	}

    public static Map<String, String> getParamsMapFromRequest(HttpServletRequest request) {
        Map<String, String> params = new HashMap<String, String>();
        Map<String,?> requestParams = request.getParameterMap();
        for (Iterator iter = requestParams.keySet().iterator(); iter.hasNext();) {
            String name = (String) iter.next();
            String[] values = (String[]) requestParams.get(name);
            String valueStr = "";
            for (int i = 0; i < values.length; i++) {
                valueStr = (i == values.length - 1) ? valueStr + values[i] : valueStr + values[i] + ",";
            }
            params.put(name, valueStr);
        }
        return params;
    }

    /**
     * 获取body的值
     * @param req
     * @return
     */
    public static String getRequestBodyStr(HttpServletRequest req) {
        StringBuilder sb = new StringBuilder();
        try (BufferedReader reader = req.getReader();) {
            char[] buff = new char[1024];
            int len;
            while ((len = reader.read(buff)) != -1) {
                sb.append(buff, 0, len);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

    public static MapBean toMapBean(HttpServletRequest request, BeanProperty[] array) {
        String r = getRequestBodyStr(request);
        if (StringUtils.isNotBlank(r)) {
            return MapBeanUtils.parseJson(r,array);
        } else {
            return null;
        }
    }
    
	
}
