package com.peans.core.common.utils;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;


public enum ListUtils {

	;
	
	/**
	 * 把list转成map
     * @param keyName 主键属性
     * @param list 集合
     * @return 返回对象
     */
    public static <T> Map<String, T> toMap(String keyName, List<T> list){
        Map<String, T> m = new HashMap<String, T>();
        try {
            for (T t : list) {
                PropertyDescriptor pd = new PropertyDescriptor(keyName,t.getClass());
                Method getMethod = pd.getReadMethod();
                Object o = getMethod.invoke(t);
                m.put(o.toString(), t);
            }
            return m;
        } catch (Exception e) {
            e.printStackTrace();
        } 
        return null;
    }
    
    
    public static <T> T[] toArray(List<T> list,T[] result){
		list.toArray(result);
		return result;
    }
	
    public static boolean isEmpty(List<?> coll) {
        return (coll == null || coll.isEmpty());
    }

    public static boolean isNotEmpty(List<?> coll) {
        return !CollectionUtils.isEmpty(coll);
    }
    
    
}
