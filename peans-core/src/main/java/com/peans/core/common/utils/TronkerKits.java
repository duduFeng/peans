package com.peans.core.common.utils;

import java.net.InetAddress;
import java.net.UnknownHostException;

import javax.servlet.http.HttpServletRequest;

import com.google.common.net.InetAddresses;

/**
 * 工具类
 * 
 * @author sf-dev
 *
 */
public class TronkerKits {

	/**
	 * 获取ip客户机的ip地址
	 * @param request
	 * @return
	 */
	public static String getIpAddr(HttpServletRequest request) {
		String ip = request.getHeader("x-forwarded-for");
		if (ip == null || !InetAddresses.isInetAddress(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || !InetAddresses.isInetAddress(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || !InetAddresses.isInetAddress(ip)) {
			ip = request.getRemoteAddr();
		}

		if (ip == null || !InetAddresses.isInetAddress(ip)) {
			try {
				ip = InetAddress.getLocalHost().getHostAddress();
			} catch (UnknownHostException e) {
				e.printStackTrace();
			}
		}

		if (ip == null || !InetAddresses.isInetAddress(ip)) {
			ip = request.getHeader("http_client_ip");
		}
		if (ip == null || !InetAddresses.isInetAddress(ip)) {
			ip = request.getHeader("HTTP_X_FORWARDED_FOR");
		}
		// 如果是多级代理，那么取第一个ip为客户ip
		if (ip != null && ip.indexOf(",") != -1) {
			ip = ip.substring(ip.lastIndexOf(",") + 1, ip.length()).trim();
		}

		if (ip == null || !InetAddresses.isInetAddress(ip)) {
			ip = "";
		}
		return ip;
	}
}
