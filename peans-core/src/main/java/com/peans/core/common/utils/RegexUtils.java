package com.peans.core.common.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 包名: com.hongma.homa.utils
 * 描 述: 正则表达式工具类
 * 创建人: Zengjie
 * 创建日期: 2015/8/19
 */

public class RegexUtils {

    /**
     * 验证正则表达式
     * @param expression 表达式
     * @param value 比较的值
     * @return 是否符合
     */
    public static boolean completeWithRegex(String expression,String value){
        Pattern p = Pattern.compile(expression);
        Matcher m = p.matcher(value);
        return m.matches();
    }

    /**
     * 检查是否为手机号码
     * @param phoneNum 手机号码
     * @return 是否为手机号码
     */
    public static boolean checkIsPhoneNum(String phoneNum){
        String expression = "^((13[0-9])|(15[^4,\\D])|(18[0,5-9]))\\d{8}$";
        boolean result = completeWithRegex(expression,phoneNum);
        return result;
    }

}
