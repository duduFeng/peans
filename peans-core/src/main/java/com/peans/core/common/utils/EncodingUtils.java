package com.peans.core.common.utils;


import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

/**
 * 包名: com.hongma.homa.utils
 * 描 述: 编码工具
 * 创建人: Zengjie
 * 创建日期: 2015/8/19
 */
public class EncodingUtils {

    /**
     * url进行Encode编码
     * @param str
     * @return
     */
    public static String urlEnodeUTF8(String str) {
        String url = null;
        try {
            url = URLEncoder.encode(str,"UTF-8");
            System.out.println(url);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return url;
    }

    /**
     * URL进行Decode解码
     * @param str
     * @return
     */
    public static String urlUnodeUTF8(String str) {
        String url = null;
        try {
            url = URLDecoder.decode(str, "UTF-8");
            System.out.println(url);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return url;
    }

}
