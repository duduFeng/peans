package com.peans.core.common.utils;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import com.google.common.base.Throwables;

/**
 * 包名: com.hongma.homa.utils
 * 描 述: bean工具类
 * 创建人: Zengjie
 * 创建日期: 2015/8/17
 */

public enum BeanUtils {
	;
	
    /**
     * 将一个 JavaBean 对象转化为一个  Map
     * @param bean 要转化的JavaBean 对象
     * @return 转化出来的  Map 对象
     * @throws IntrospectionException 如果分析类属性失败
     * @throws IllegalAccessException 如果实例化 JavaBean 失败
     * @throws InvocationTargetException 如果调用属性的 setter 方法失败
     */
    public static Map<String,Object> toMap(Object bean)
            throws IntrospectionException, IllegalAccessException, InvocationTargetException {
        Class<?> type = bean.getClass();
        Map<String,Object> returnMap = new HashMap<String,Object>();
        BeanInfo beanInfo = Introspector.getBeanInfo(type);
        PropertyDescriptor[] propertyDescriptors =  beanInfo.getPropertyDescriptors();
        for (int i = 0; i< propertyDescriptors.length; i++) {
            PropertyDescriptor descriptor = propertyDescriptors[i];
            String propertyName = descriptor.getName();
            if (!propertyName.equals("class")) {
                Method readMethod = descriptor.getReadMethod();
                Object result = readMethod.invoke(bean, new Object[0]);
                returnMap.put(propertyName, result);
            }
        }
        return returnMap;
    }
    

    public static Object cloneBean(Object bean) {
        try {
            return org.apache.commons.beanutils.BeanUtils.cloneBean(bean);
        } catch (Exception e) {
        	Throwables.propagate(e);
        	return null;
        }
    }

    public static void copyProperties(Object dest, Object orig) {
        try {
            org.apache.commons.beanutils.BeanUtils.copyProperties(dest,orig);
        } catch (Exception e) {
        	Throwables.propagate(e);
        }
    }

    public static void copyProperty(Object bean, String name, Object value){
        try {
            org.apache.commons.beanutils.BeanUtils.copyProperty(bean,name,value);
        } catch (Exception e) {
        	Throwables.propagate(e);
        }
    }


    public static Map<String, String> describe(Object bean){
        try {
            return org.apache.commons.beanutils.BeanUtils.describe(bean);
        } catch (Exception e) {
        	Throwables.propagate(e);
        	return null;
        }
    }

    public static String[] getArrayProperty(Object bean, String name){
        try {
            return org.apache.commons.beanutils.BeanUtils.getArrayProperty(bean,name);
        } catch (Exception e) {
        	Throwables.propagate(e);
        	return null;
        }
    }

    public static String getIndexedProperty(Object bean, String name){
        try {
            return org.apache.commons.beanutils.BeanUtils.getIndexedProperty(bean,name);
        } catch (Exception e) {
        	Throwables.propagate(e);
        	return null;
        }
    }

    public static String getIndexedProperty(Object bean, String name, int index){
        try {
            return org.apache.commons.beanutils.BeanUtils.getIndexedProperty(bean,name,index);
        } catch (Exception e) {
        	Throwables.propagate(e);
        	return null;
        }

    }

    public static String getMappedProperty(Object bean, String name){
        try {
            return org.apache.commons.beanutils.BeanUtils.getMappedProperty(bean,name);
        } catch (Exception e) {
        	Throwables.propagate(e);
        	return null;
        }
    }

    public static String getMappedProperty(Object bean,String name, String key) {
        try {
            return org.apache.commons.beanutils.BeanUtils.getMappedProperty(bean,name,key);
        } catch (Exception e) {
        	Throwables.propagate(e);
        	return null;
        }
    }

    public static String getNestedProperty(Object bean, String name){
        try {
            return org.apache.commons.beanutils.BeanUtils.getNestedProperty(bean,name);
        } catch (Exception e) {
        	Throwables.propagate(e);
        	return null;
        }
    }

    public static String getProperty(Object bean, String name){
        try {
            return org.apache.commons.beanutils.BeanUtils.getProperty(bean,name);
        } catch (Exception e) {
        	Throwables.propagate(e);
        	return null;
        }
    }

    public static String getSimpleProperty(Object bean, String name){
        try {
            return org.apache.commons.beanutils.BeanUtils.getSimpleProperty(bean,name);
        } catch (Exception e) {
        	Throwables.propagate(e);
        	return null;
        }
    }

    public static void populate(Object bean, Map<String, ? extends Object> properties)
            throws IllegalAccessException, InvocationTargetException {
        try {
            org.apache.commons.beanutils.BeanUtils.populate(bean,properties);
        } catch (Exception e) {
        	Throwables.propagate(e);
        }
    }

    public static void setProperty(Object bean, String name, Object value){
        try {
            org.apache.commons.beanutils.BeanUtils.setProperty(bean,name,value);
        } catch (Exception e) {
        	Throwables.propagate(e);
        }
    }
}
