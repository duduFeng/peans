package com.peans.core.common.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.Period;
import org.joda.time.PeriodType;

public enum DateUtils {
	;

	public static int getAge(Date date){
		DateTime end = new DateTime();
		DateTime begin = new DateTime(date);
		Period p = new Period(begin, end, PeriodType.years());
		return p.getYears();
	}

	public final static String time_day_format_str = "yyyy-MM-dd";
	public final static String time_minute_format_str = "yyyy-MM-dd HH:mm:ss";
	public final static String time_second_format_str = "yyyy-MM-dd HH:mm:ss";
	public final static String time_millSecond_format_str = "yyyy-MM-dd HH:mm:ss.SSS";


	/**
	 * 转化日期格式(day)
	 * @param date
	 * @return
	 */
	public static String formatOfDay(Date date){
		return formatDate(date,time_day_format_str);
	}

	/**
	 * 转化日期格式(minute)
	 * @param date
	 * @return
	 */
	public static String formatOfMinute(Date date){
		return formatDate(date,time_minute_format_str);
	}

	/**
	 * 转化日期格式(second)
	 * @param date
	 * @return
	 */
	public static String formatOfSeconds(Date date){
		return formatDate(date,time_second_format_str);
	}

	/**
	 * 转化日期格式(millSecond)
	 * @param date
	 * @return
	 */
	public static String formatOfMiillSecond(Date date){
		return formatDate(date,time_millSecond_format_str);
	}


	/**
	 * 转化日期格式
	 * @param date
	 * @param formatStr
	 * @return
	 */
	public static String formatDate(Date date,String formatStr){
		DateTime t = new DateTime(date);
		//SimpleDateFormat sf=new SimpleDateFormat(formatStr);
		return t.toString(formatStr);
	}

	/**
	 * 转化成日期str格式
	 * @param source
	 * @param pattern
	 * @return
	 */
	public static Date parse(String source,String pattern){
		SimpleDateFormat sf=new SimpleDateFormat(pattern);
		try {
			return sf.parse(source);
		} catch (ParseException e) {
			throw new IllegalArgumentException("格式错误");
		}
	}

	/**
	 * 转化成日期字符串格式（day）
	 * @param source
	 * @return
	 */
	public static Date parseOfDay(String source){
		return parse(source,time_day_format_str);
	}

	/**
	 * 转化成日期字符串格式（day）
	 * @param source
	 * @return
	 */
	public static Date parseOfMinute(String source){
		return parse(source,time_minute_format_str);
	}

	/**
	 * 转化成日期字符串格式（millisecond）
	 * @param source
	 * @return
	 */
	public static Date parseTimeOfMillisecond(String source){
		return parse(source,time_millSecond_format_str);
	}

	/**
	 * 转化成日期字符串格式(second）
	 * @param source
	 * @return
	 */
	public static Date parseTimeOfSecond(String source){
		return parse(source,time_second_format_str);
	}

	/**
	 * 增加或减少年
	 * @param date
	 * @param years
	 * @return
	 */
	public static Date plusYeas(Date date,int years){
		DateTime dateTime = new DateTime(date);
		dateTime = dateTime.plusYears(years);
		return dateTime.toDate();
	}

	/**
	 * 增加或减少月
	 * @param date
	 * @param month
	 * @return
	 */
	public static Date plusMonths(Date date,int month){
		DateTime dateTime = new DateTime(date);
		dateTime = dateTime.plusMonths(month);
		return dateTime.toDate();
	}

	/**
	 * 增加或减少天
	 * @param date
	 * @param days
	 * @return
	 */
	public static Date plusdays(Date date,int days){
		DateTime dateTime = new DateTime(date);
		dateTime = dateTime.plusDays(days);
		return dateTime.toDate();
	}

	/**
	 * 增加或减少分钟
	 * @param date
	 * @param days
	 * @return
	 */
	public static Date plusMinutes(Date date,int minutes){
		DateTime dateTime = new DateTime(date);
		dateTime = dateTime.plusMinutes(minutes);
		return dateTime.toDate();
	}

	/**
	 * 增加或减少秒
	 * @param date
	 * @param days
	 * @return
	 */
	public static Date plusSeconds(Date date,int seconds){
		DateTime dateTime = new DateTime(date);
		dateTime = dateTime.plusSeconds(seconds);
		return dateTime.toDate();
	}

	/**
	 * 相差天数
	 * @param date1
	 * @param date2
	 * @return
	 */
	public static int getDayPeriod(Date date1,Date date2){
		DateTime d1 = new DateTime(date1);
		DateTime d2 = new DateTime(date2);
		Period p = new Period(d1, d2, PeriodType.days());
		int days = p.getDays();
		return days;
	}

	/**
	 * 获取当前时间
	 * @return
	 */
	public static String getNowtimeStr(){
		return formatOfSeconds(new Date());
	}

	public static int getEra(Date date){
		return new DateTime(date).getEra();
	}

	public static int getCenturyOfEra(Date date){
		return new DateTime(date).getCenturyOfEra();
	}

	public static  int getYearOfEra(Date date){
		return new DateTime(date).getYearOfEra();
	}

	public static int getYearOfCentury(Date date){
		return new DateTime(date).getYearOfCentury();
	}

	public static  int getYear(Date date){
		return new DateTime(date).getYear();
	}

	public static  int getWeekyear(Date date){
		return new DateTime(date).getWeekyear();
	}

	public static int getMonthOfYear(Date date){
		return new DateTime(date).getMonthOfYear();
	}

	public static int getWeekOfWeekyear(Date date){
		return new DateTime(date).getWeekOfWeekyear();
	}

	public static int getDayOfMonth(Date date){
		return new DateTime(date).getDayOfMonth();
	}

	public static int getDayOfYear(Date date){
		return new DateTime(date).getDayOfYear();
	}

	public static  int getDayOfWeek(Date date){
		return new DateTime(date).getDayOfWeek();
	}

	public static  int getHourOfDay(Date date){
		return new DateTime(date).getHourOfDay();
	}

	public static  int getMinuteOfDay(Date date){
		return new DateTime(date).getMinuteOfDay();
	}

	public static  int getMinuteOfHour(Date date){
		return new DateTime(date).getMinuteOfHour();
	}

	public static  int getSecondOfDay(Date date){
		return new DateTime(date).getSecondOfDay();
	}

	public static  int getSecondOfMinute(Date date){
		return new DateTime(date).getSecondOfMinute();
	}

	public static int getMillisOfDay(Date date){
		return new DateTime(date).getMillisOfDay();
	}

	public static int getMillisOfSecond(Date date){
		return new DateTime(date).getMillisOfSecond();
	}


	public static String getIntervalTime(Date date){
		String result = "";
		DateTime end = new DateTime();
		DateTime begin = new DateTime(date);
		Duration d = new Duration(begin, end);
		int[] arr = new int[]{1,24,7*24,4*7*24,365*24};
		int index = 0;
		for (int i = 0; i < arr.length; i++) {
			if(d.getStandardHours() >= arr[i]){
				index = index + 1;
			}
		}
		switch (index) {
		case 0:
			result = d.getStandardMinutes()+"分前";
			break;
		case 1:
			result = d.getStandardHours()+"小时前";
			break;
		case 2:
			result = d.getStandardDays()+"天前";
			break;
		case 3:
			Period pw = new Period(begin, end, PeriodType.weeks());
			result = pw.getWeeks()+"周前";
			break;
		case 4:
			Period pm = new Period(begin, end, PeriodType.months());
			if(pm.getMonths() > 0){
				result = pm.getMonths()+"月前";
			}else{
				result = "4周前";
			}
			break;
		case 5:
			Period py = new Period(begin, end, PeriodType.years());
			result = py.getYears()+"年前";
			break;
		default:
			break;
		}
		return result;
	}

	public static boolean isBefore(Date date1, Date date2) {
		DateTime t1 = new DateTime(date1);
		DateTime t2 = new DateTime(date2);
		return t1.isBefore(t2);
	}

	public static boolean isAfter(Date date1, Date date2) {
		DateTime t1 = new DateTime(date1);
		DateTime t2 = new DateTime(date2);
		return t1.isAfter(t2);
	}

	public static Date getMinOfDate(Date date) {
		DateTime t1 = new DateTime(date);
		DateTime t2 = new DateTime(t1.getYear(),t1.getMonthOfYear(),t1.getDayOfMonth(),0,0,0,0);
		return t2.toDate();
	}
	
	public static Date getMaxOfDate(Date date) {
		DateTime t1 = new DateTime(date);
		DateTime t2 = new DateTime(t1.getYear(),t1.getMonthOfYear(),t1.getDayOfMonth(),23,59,59,59);
		return t2.toDate();
	}
	
	public static Date getMinOfToday() {
		DateTime t1 = DateTime.now();
		DateTime t2 = new DateTime(t1.getYear(),t1.getMonthOfYear(),t1.getDayOfMonth(),0,0,0,0);
		return t2.toDate();
	}
	
	public static Date getMaxOfToday() {
		DateTime t1 = DateTime.now();
		DateTime t2 = new DateTime(t1.getYear(),t1.getMonthOfYear(),t1.getDayOfMonth(),23,59,59,59);
		return t2.toDate();
	}
	public static int daysBetween(Date smdate,Date bdate)   
	    {    
	        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");  
	    try {
			smdate=sdf.parse(sdf.format(smdate));
			bdate=sdf.parse(sdf.format(bdate));  
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
	    Calendar cal = Calendar.getInstance();    
	    cal.setTime(smdate);    
	    long time1 = cal.getTimeInMillis();                 
	    cal.setTime(bdate);    
	    long time2 = cal.getTimeInMillis();         
	    long between_days=(time2-time1)/(1000*3600*24);  
	        
	   return Integer.parseInt(String.valueOf(between_days));           
	} 
	
	public static void main(String[] args) {
		Date date1=org.apache.commons.lang3.time.DateUtils.addDays(new Date(), 1);
		System.out.println(DateUtils.formatDate(date1, "yyyy-MM-dd"));
		System.out.println(DateUtils.daysBetween(new Date(),DateUtils.parseOfDay("2016-06-28")));
		
		System.out.println(DateUtils.getDayPeriod(new Date(),DateUtils.parseOfDay("2016-6-28")));
	}

}
