package com.peans.core.common.utils.mapBean;

/**
 * Created by res-miaofeng on 2016/3/2.
 */
public interface BeanProperties {
    BeanProperty[] list();
}
