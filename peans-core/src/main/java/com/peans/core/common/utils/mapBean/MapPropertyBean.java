package com.peans.core.common.utils.mapBean;

public class MapPropertyBean extends MapBean {

	private BeanProperty[] properties;
	
    public MapPropertyBean(BeanProperty[] properties){
        super();
        this.properties = properties;
     }

	public BeanProperty[] getProperties() {
		return properties;
	}
    
}
