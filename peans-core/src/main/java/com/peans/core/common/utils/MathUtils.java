package com.peans.core.common.utils;

import java.math.BigDecimal;

/**
 * 包 名: com.peans.common.utils
 * 描 述:
 * 创建人: nickfeng
 * 创建日期: 2015/10/9
 */
public enum MathUtils {
	;
	/**
	 * 加法运算
	 * @param b1
	 * @param b2
	 * @return
	 */
    public static BigDecimal add(BigDecimal b1, BigDecimal b2) {
        return b1.add(b2);
    }

    /**
     * 加法运算
     * @param b1
     * @param b2
     * @param b3
     * @return
     */
    public static BigDecimal add(BigDecimal b1, BigDecimal b2,BigDecimal b3) {
        return b1.add(b2).add(b3);
    }

    /**
     * 减法运算
     * @param b1
     * @param b2
     * @return
     */
    public static BigDecimal sub(BigDecimal b1, BigDecimal b2) {
        return b1.subtract(b2);
    }

    /**
     * 乘法运算
     * @param b1
     * @param b2
     * @return
     */
    public static BigDecimal mul(BigDecimal b1, BigDecimal b2) {
        return b1.multiply(b2);
    }

    /**
     * 乘法运算
     * @param b1
     * @param b2
     * @param b3
     * @return
     */
    public static BigDecimal mul(BigDecimal b1, BigDecimal b2,BigDecimal b3) {
        return b1.multiply(b2).multiply(b3);
    }

    /**
     * 乘法运算
     * @param b1
     * @param b2
     * @param b3
     * @param b4
     * @return
     */
    public static BigDecimal mul(BigDecimal b1, BigDecimal b2,BigDecimal b3,BigDecimal b4) {
        return b1.multiply(b2).multiply(b3).multiply(b4);
    }

    /**
     * 乘法运算
     * @param b1
     * @param b2
     * @return
     */
    public static BigDecimal divide(BigDecimal b1, BigDecimal b2) {
        return b1.divide(b2);
    }

    /**
     * 保留两位小数
     * @param b1
     * @return
     */
    public static  BigDecimal scale2(BigDecimal b1){
        return b1.setScale(BigDecimal.ROUND_HALF_EVEN);
    }

    /**
     * 等额本息，月平均还款
     * 月平均还款 ＝贷款额×利率×（1＋利率）^总期数÷〔（1＋利率）^总期数－1〕
     * @param principal
     * @param rate
     * @param periods
     * @return
     */
    public static BigDecimal loadAverageCapitalPlusInterest(BigDecimal principal,BigDecimal rate,Integer periods){
        BigDecimal t = rate.add(new BigDecimal(1)).pow(periods);
        BigDecimal a = principal.multiply(rate).multiply(t);
        BigDecimal result = a.divide(t.subtract(new BigDecimal(1)),8,BigDecimal.ROUND_HALF_EVEN);
        return result;
    }


    /**
     * 等额本息，月付利息
     * 月付利息=（a×i－b）×（1＋i）^（n－1）＋b
     * @param a 贷款总额
     * @param i 利率
     * @param n 第几期
     * @param b 月平均还款
     * @return
     */
    public static BigDecimal loadAverageCapitalPlusInterestPayInterest(BigDecimal a,BigDecimal i,Integer n,BigDecimal b){
        BigDecimal r1 = a.multiply(i).subtract(b);
        BigDecimal r2 = i.add(new BigDecimal(1)).pow(n-1);
        BigDecimal result = r1.multiply(r2).add(b);
        return result.setScale(8,BigDecimal.ROUND_HALF_EVEN);
    }



    public static void main(String[] args) {
        BigDecimal result = MathUtils.loadAverageCapitalPlusInterest(new BigDecimal(10000),new BigDecimal(0.01),12);
        System.out.println(result.setScale(2,BigDecimal.ROUND_HALF_EVEN));
        BigDecimal all = new BigDecimal(0.0);
        for (int i = 1; i < 13; i++) {
            BigDecimal r = MathUtils.loadAverageCapitalPlusInterestPayInterest(new BigDecimal(10000),new BigDecimal(0.01),i,result);
            all = all.add(r);
            //System.out.println(r);
            System.out.println(r.setScale(2,BigDecimal.ROUND_HALF_EVEN));
        }
        System.out.println(all.setScale(2,BigDecimal.ROUND_HALF_EVEN));
    }

}
