package com.peans.core.protocol;

import java.util.List;

/**
 * Created by nickfeng on 2017/5/25.
 */
public class RspListVo<T>  {

    /**
     * 0.表示成功。
     * 1.约定负数的code是全局统一挺好理。
     * 2.code前两位代表业务模块的编码，后三位为自定义code。
     */
    private String code;//

    private String msg;//返回的消息，如success,fail等

    private String prompt;//向用户展示的提示信息

    private List<T> data;

    private Integer total;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getPrompt() {
        return prompt;
    }

    public void setPrompt(String prompt) {
        this.prompt = prompt;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }
}
