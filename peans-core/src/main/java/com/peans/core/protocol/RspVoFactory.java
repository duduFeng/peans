package com.peans.core.protocol;

import org.apache.commons.lang3.StringUtils;

import java.util.List;

/**
 * Created by nickfeng on 2017/5/25.
 */
public enum RspVoFactory {

    ;

    public static RspVo success(String prompt,Object data,String msg) {
        RspVo vo = new RspVo();
        vo.setCode("00000");
        vo.setData(data);
        if (StringUtils.isNotBlank(msg)) {
            vo.setMsg(msg);
        } else {
            vo.setMsg("success");
        }
        if (StringUtils.isNotBlank(prompt)) {
            vo.setPrompt(prompt);
        } else {
            vo.setPrompt("操作成功");
        }
        return vo;
    }

    public static RspVo success(Object data) {
        return success("",data,"");
    }

    public static RspListVo successList(List<?> data) {
        RspListVo vo = new RspListVo();
        vo.setCode("00000");
        vo.setData(data);
        vo.setTotal(data.size());
        vo.setMsg("suceess");
        vo.setPrompt("");
        return vo;
    }

    public static RspListVo successPage(PageQueryVo page) {
        RspListVo vo = new RspListVo();
        vo.setCode("00000");
        vo.setData(page.getContent());
        vo.setTotal(page.getTotal());
        vo.setMsg("suceess");
        vo.setPrompt("");
        return vo;
    }

    public static RspVo successWithPrompt(String prompt) {
        RspVo vo = new RspVo();
        vo.setCode("00000");
        vo.setData(null);
        vo.setMsg("suceess");
        vo.setPrompt(prompt);
        return vo;
    }

    public static RspVo successWithPrompt(String prompt,Object data) {
        RspVo vo = new RspVo();
        vo.setCode("00000");
        vo.setData(data);
        vo.setMsg("suceess");
        vo.setPrompt(prompt);
        return vo;
    }

    public static RspVo fail(String prompt,String code,Object data,String msg) {
        RspVo vo = new RspVo();
        if (StringUtils.isNotBlank(code)) {
            vo.setCode(code);
        } else {
            vo.setCode("99999");
        }
        vo.setData(data);
        if (StringUtils.isNotBlank(msg)) {
            vo.setMsg(msg);
        } else {
            vo.setMsg("fail");
        }
        if (StringUtils.isNotBlank(prompt)) {
            vo.setPrompt(prompt);
        } else {
            vo.setPrompt("操作失败");
        }
        return vo;
    }

    public static RspVo failWithPrompt(String prompt,Object data) {
        return fail(prompt,"",data,"");
    }

    public static RspVo fail(Object data) {
        return fail("","",data,"");
    }

}
