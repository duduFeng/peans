package com.peans.core.protocol;

import java.util.List;

/**
 * Created by nickfeng on 2017/5/25.
 */
public class PageQueryVo<T> {

    private Integer total;

    private Integer limit;

    private Integer skip;

    private List<T> content;

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getSkip() {
        return skip;
    }

    public void setSkip(Integer skip) {
        this.skip = skip;
    }

    public List<T> getContent() {
        return content;
    }

    public void setContent(List<T> content) {
        this.content = content;
    }
}
