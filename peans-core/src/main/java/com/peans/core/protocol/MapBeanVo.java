package com.peans.core.protocol;

import com.peans.core.common.utils.JsonUtils;
import com.peans.core.common.utils.mapBean.MapBean;

public class MapBeanVo extends MapBean implements ProtocolVo,OpenProtocol{

	@Override
	public String toJsonStr() {
		return JsonUtils.beanToJson(this);
	}

}
