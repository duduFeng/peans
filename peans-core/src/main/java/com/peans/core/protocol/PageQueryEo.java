package com.peans.core.protocol;

/**
 * Created by nickfeng on 2017/5/25.
 */
public abstract class PageQueryEo {

    private Integer limit;

    private Integer skip;

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getSkip() {
        return skip;
    }

    public void setSkip(Integer skip) {
        this.skip = skip;
    }
}
