package com.peans.core.component;

import com.peans.core.annotation.SignRequired;
import com.peans.core.common.utils.Md5Sign;
import com.peans.core.common.utils.ServletUtils;
import com.peans.core.exception.SignFailException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;
import java.util.Map;

/**
 * 访问参数拦截器
 */
public class ReqParamsInterceptor extends HandlerInterceptorAdapter {

    private static final Logger logger = LoggerFactory.getLogger(ReqParamsInterceptor.class);

    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response, Object handler) throws Exception {

        HandlerMethod handlerMethod = (HandlerMethod) handler;
        Method method = handlerMethod.getMethod();
        SignRequired annotation = method.getAnnotation(SignRequired.class);
        if (annotation != null) {
            checkSign(request);
        }
        return true;
    }

    private void checkSign(HttpServletRequest request) {
        Map<String, String> params = ServletUtils.getParamsMapFromRequest(request);
        if (params.containsKey("sign")&&params.containsKey("accessUserId")) {
            checkSign(params);
        }else {
            throw new IllegalArgumentException("sign或accessUserId参数名为空");
        }
    }

    public void checkSign(Map<String, String> params) {
        String accessUserId = params.get("accessUserId");
        String sign = params.get("sign");
        if (StringUtils.isEmpty(sign)) {
            throw new IllegalArgumentException("没有签名字符串或者值为空！");
        }
        String sign2 = Md5Sign.buildSign(params,"xxx");// TODO: 2016/8/25
        logger.info("签名值:"+sign2);
        if (!sign.equals(sign2)) {
            logger.error("md5签名失败!");
            throw new SignFailException("sign_fail!");
        }
    }

}
