package com.peans.core.component;

import com.peans.core.annotation.RequestLog;
import com.peans.core.common.utils.JsonUtils;
import com.peans.core.common.utils.ServletUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;
import java.util.Map;

/**
 * 访问参数拦截器
 */
public class RequestLogInterceptor extends HandlerInterceptorAdapter {

    private static final Logger logger = LoggerFactory.getLogger(RequestLogInterceptor.class);

    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response, Object handler) throws Exception {

        HandlerMethod handlerMethod = (HandlerMethod) handler;
        Method method = handlerMethod.getMethod();
        RequestLog annotation = method.getAnnotation(RequestLog.class);
        if (annotation != null) {
            try {
                logRequest(request);
            } catch (Exception e) {
                e.printStackTrace();
            }
            //return false;
        }
        return true;
    }

    private void logRequest(HttpServletRequest request) {
        Map<String, String> params = ServletUtils.getParamsMapFromRequest(request);
        StringBuilder sb = new StringBuilder();
        sb.append("\n接到请求：");
        sb.append("\n\t");
        sb.append("url:"+request.getRequestURL());
        sb.append("\n\t");
        sb.append("params:"+ JsonUtils.beanToJson(params));
        logger.info(sb.toString());
    }

}
