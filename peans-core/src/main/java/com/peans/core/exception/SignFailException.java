package com.peans.core.exception;

public class SignFailException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1637252988017505102L;
	
    public SignFailException() {
        super();
    }

    public SignFailException(String message) {
        super(message);
    }

    public SignFailException(String message, Throwable cause) {
        super(message, cause);
    }

    public SignFailException(Throwable cause) {
        super(cause);
    }

}
