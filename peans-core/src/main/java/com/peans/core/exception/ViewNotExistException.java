package com.peans.core.exception;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by sf-dev on 2016/1/25.
 */
public final class ViewNotExistException extends RuntimeException {

    private Map<String, Object[]> messages = new LinkedHashMap<>();

    public ViewNotExistException() {
    }

    public ViewNotExistException(String message) {
        super(message);
        this.messages.put(message, new Object[0]);
    }

    public ViewNotExistException(String message, Object... args) {
        super(message);
        this.messages.put(message, args);
    }

    public ViewNotExistException(Map<String, Object[]> messages) {
        this.messages = messages;
    }

    public Map<String, Object[]> getMessages() {
        return messages;
    }
}