package com.peans.core.core.test.test;

import com.google.common.collect.Lists;
import com.peans.core.common.utils.JsonUtils;

import java.util.Date;
import java.util.List;

public class TestJsonUtils {
	
	public static void main(String[] args) {
		BeanA a = new BeanA();
		a.setAge("11");
		a.setName("feng");
		a.setDate(new Date());
		
		BeanA b = new BeanA();
		b.setAge("22");
		b.setName("feng2");
		b.setDate(new Date());
		
		List<BeanA> list = Lists.newArrayList(a,b);
		
		String str = JsonUtils.arrayToJson(list);
		System.out.println(JsonUtils.arrayToJson(list));
		
		List<BeanA> list2 = (List<BeanA>) JsonUtils.jsonToArray(str,BeanA.class);
		System.out.println(list2);
	}

}
