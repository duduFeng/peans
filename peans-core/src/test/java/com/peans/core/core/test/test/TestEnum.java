package com.peans.core.core.test.test;

import com.peans.core.common.utils.mapBean.BeanProperty;
import com.peans.core.common.utils.mapBean.MapBean;

import java.util.Date;

/**
 * Created by res-miaofeng on 2016/3/2.
 */
public enum TestEnum implements BeanProperty {
    key(String.class),
    te(Integer.class),
    testBean(TestBean.class),
    map(MapBean.class),
    date(Date.class),
    aa(String.class);

    private Class<?> classType;

    private TestEnum(Class<?> classType){
        this.classType = classType;
    }

    @Override
    public String getName() {
        return name();
    }

    @Override
    public Class<?> getClassType() {
        return classType;
    }

}
