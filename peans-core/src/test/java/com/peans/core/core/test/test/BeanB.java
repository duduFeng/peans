package com.peans.core.core.test.test;

public class BeanB {

	private String nameB;
	
	private String ageB;

	public String getNameB() {
		return nameB;
	}

	public void setNameB(String nameB) {
		this.nameB = nameB;
	}

	public String getAgeB() {
		return ageB;
	}

	public void setAgeB(String ageB) {
		this.ageB = ageB;
	}
	
	
	
}
