package com.peans.core.core.test;

import java.lang.reflect.Field;
import java.util.Map;
import java.util.Map.Entry;

import com.peans.core.annotation.Description;
import com.peans.core.common.utils.ReflectUtils;

public class DescriptionMain {

	public static void test(Class<?> cl) {
		Map<String, Field> map = ReflectUtils.getClassFields(cl,true);
		if (map != null && map.size() > 0) {
		     for (Entry<String, Field> f : map.entrySet()) {
		    	 Description d = f.getValue().getAnnotation(Description.class);
		         if (d != null) {
		             System.out.println(d.value());
		         }
		     }
		}
	 }
	
	public static void main(String[] args) {
		DescriptionMain.test(TestBean.class);
	}
}
