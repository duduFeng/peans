package com.peans.core.core.test.test;

/**
 * Created by res-miaofeng on 2016/3/2.
 */
public class TestBean {

    private String name;

    private String aa;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAa() {
        return aa;
    }

    public void setAa(String aa) {
        this.aa = aa;
    }
}
