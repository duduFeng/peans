package com.peans.core.core.test;

import com.peans.core.annotation.Description;

public class TestBean extends AbstractTestBean{

	@Description("aa1des")
	private String aa1;
	
	@Description("aa2des")
	private Integer aa2;

	public String getAa1() {
		return aa1;
	}

	public void setAa1(String aa1) {
		this.aa1 = aa1;
	}

	public Integer getAa2() {
		return aa2;
	}

	public void setAa2(Integer aa2) {
		this.aa2 = aa2;
	}
	
	
	
}
