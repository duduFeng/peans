package com.peans.core.core.test.test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.peans.core.common.utils.mapBean.MapBean;
import com.peans.core.common.utils.mapBean.MapBeanUtils;
import com.peans.core.common.utils.mapBean.MapPropertyBean;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by res-miaofeng on 2016/3/2.
 */
public class TestMapBean {


    public static void main(String[] args) {
        MapBean map = new MapBean();
        map.add(TestEnum.aa,"aaaa");
        map.add(TestEnum.key,"bbbbb");
        map.add(TestEnum.te,11);

        TestBean bean = new TestBean();
        bean.setName("name");
        bean.setAa("aaaaaa");

        //map.add(TestEnum.testBean,bean);
        MapBean map1 = new MapBean();
        map1.add(TestEnum.aa,"aaaa");
        map1.add(TestEnum.key,"bbbbb");
        map.add(TestEnum.map,map1);

        map.add(TestEnum.date,new Date());

        //MapPropertyBean bean2 = new MapPropertyBean(TestEnum.values());
        //bean2.putAll(map);
        
        //System.out.println(map.get(TestEnum.aa));
        ObjectMapper mapper = new ObjectMapper();
        DateFormat s = new SimpleDateFormat("yyyy-MM-dd");
        mapper.setDateFormat(s);
        try {
            String value = mapper.writeValueAsString(map);
            System.out.println(value);
            MapBean bb = MapBeanUtils.parseJson(value,TestEnum.values());
            System.out.println(bb);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }


}
