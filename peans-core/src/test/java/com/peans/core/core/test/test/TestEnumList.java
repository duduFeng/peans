package com.peans.core.core.test.test;

import com.peans.core.common.utils.mapBean.BeanProperties;
import com.peans.core.common.utils.mapBean.BeanProperty;

/**
 * Created by res-miaofeng on 2016/3/2.
 */
public enum TestEnumList implements BeanProperties {
	;

	@Override
	public BeanProperty[] list() {
		return TestEnum.values();
	}
    

}
