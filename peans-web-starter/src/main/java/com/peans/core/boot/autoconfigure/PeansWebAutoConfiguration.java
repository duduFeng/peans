package com.peans.core.boot.autoconfigure;

import com.peans.core.component.MapParamResolver;
import com.peans.core.component.ReqParamsInterceptor;
import com.peans.core.component.RequestLogInterceptor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.util.List;

/**
 * Created by admin on 2016/10/10.
 */
@Configuration
public class PeansWebAutoConfiguration extends WebMvcConfigurerAdapter {

    private static final Log logger = LogFactory.getLog(PeansWebAutoConfiguration.class);

    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
        super.addArgumentResolvers(argumentResolvers);
        MapParamResolver a = new MapParamResolver();
        argumentResolvers.add(a);
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        super.addInterceptors(registry);
        ReqParamsInterceptor reqParamsInterceptor = new ReqParamsInterceptor();
        RequestLogInterceptor requestLogInterceptor = new RequestLogInterceptor();
        registry.addInterceptor(reqParamsInterceptor);
        registry.addInterceptor(requestLogInterceptor);
    }



}
