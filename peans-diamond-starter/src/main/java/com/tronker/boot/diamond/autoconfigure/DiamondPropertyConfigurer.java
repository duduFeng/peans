package com.tronker.boot.diamond.autoconfigure;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.StringReader;
import java.text.MessageFormat;
import java.util.Enumeration;
import java.util.Properties;

/**
 * Created by admin on 2016/10/19.
 */
public class DiamondPropertyConfigurer {
    private static Logger logger = Logger.getLogger(DiamondPropertyConfigurer.class);

    public static Properties props = null;

    public static void load(StringReader reader){
        if(null == props)
            props = new Properties();
        try {
            props.load(reader);
        } catch (IOException e) {
            logger.error(e);
        }
    }

    public static void load(Properties defaultProps){
        if(null == props) {
            props = new Properties();
            convertProperties(defaultProps);
        }
        else {
            convertProperties(defaultProps);
        }
    }

    public static void convertProperties(Properties defaultProps) {
        Enumeration<?> propertyNames = defaultProps.propertyNames();
        while (propertyNames.hasMoreElements()) {
            String propertyName = (String) propertyNames.nextElement();
            String propertyValue = defaultProps.getProperty(propertyName);
            if (!StringUtils.isEmpty(propertyName)) {
                props.setProperty(propertyName, propertyValue);
            }
        }
    }

    public static Object getProperty(String key) {
        if(null == props)
            return null;
        else
            return props.get(key);
    }

    public static String getValue(String key) {
        Object object = getProperty(key);
        if(null != object) {
            return (String)object;
        }
        else {
            logger.warn("������Ϊ" + key + "������δ��Diamond����ӻ����õ�����Ϊ��");
            return null;
        }
    }

    public static String getValue(String key, String defaultValue) {
        Object object = getProperty(key);
        if(null != object) {
            return (String)object;
        }
        else {
            logger.warn("������Ϊ" + key + "������δ��Diamond����ӻ����õ�����Ϊ��");
            return defaultValue;
        }
    }

    public static String getString(String key) {
        Object object = getProperty(key);
        if(null != object) {
            return (String)object;
        }
        else {
            logger.warn("������Ϊ" + key + "������δ��Diamond����ӻ����õ�����Ϊ��");
            return null;
        }
    }

    public static String getString(String key, String defaultString) {
        Object object = getProperty(key);
        if(null != object) {
            return (String)object;
        }
        else {
            logger.warn("������Ϊ" + key + "������δ��Diamond����ӻ����õ�����Ϊ��");
            return defaultString;
        }
    }

    public static Long getLong(String key) {
        Object object = getProperty(key);
        if(null != object)
            return Long.parseLong(object.toString());
        else {
            logger.warn("������Ϊ" + key + "������δ��Diamond����ӻ����õ�����Ϊ��");
            return null;
        }
    }

    public static Long getLong(String key, long defaultLong) {
        Object object = getProperty(key);
        if(null != object)
            return Long.parseLong(object.toString());
        else {
            logger.warn("������Ϊ" + key + "������δ��Diamond����ӻ����õ�����Ϊ��");
            return defaultLong;
        }
    }

    public static Integer getInteger(String key) {
        Object object = getProperty(key);
        if(null != object) {
            return Integer.parseInt(object.toString());
        }
        else {
            logger.warn("������Ϊ" + key + "������δ��Diamond����ӻ����õ�����Ϊ��");
            return null;
        }
    }

    public static Integer getInteger(String key, int defaultInt) {
        Object object = getProperty(key);
        if(null != object) {
            return Integer.parseInt(object.toString());
        }
        else {
            logger.warn("������Ϊ" + key + "������δ��Diamond����ӻ����õ�����Ϊ��");
            return defaultInt;
        }
    }

    public static String getString(String key, Object[] array) {
        String message = getValue(key);
        if(null != message) {
            return MessageFormat.format(message, array);
        }
        else {
            return null;
        }
    }

    public static String getValue(String key, Object... array) {
        String message = getValue(key);
        if(null != message) {
            return MessageFormat.format(message, array);
        }
        else {
            return null;
        }
    }


}
