package com.tronker.boot.diamond.autoconfigure;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;

/**
 * Created by admin on 2016/9/20.
 */
public class DiamondRunner implements CommandLineRunner {

    protected final Logger logger = LoggerFactory.getLogger(DiamondRunner.class);

    @Override
    public void run(String... args) throws Exception {
        System.out.println("钻石自动配置启动成功.......");
        logger.info("钻石自动配置启动成功.......");
    }
}
