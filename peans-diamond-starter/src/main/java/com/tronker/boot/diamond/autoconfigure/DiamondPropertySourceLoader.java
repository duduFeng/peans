package com.tronker.boot.diamond.autoconfigure;

import com.taobao.diamond.manager.DiamondManager;
import com.taobao.diamond.manager.ManagerListener;
import com.taobao.diamond.manager.impl.DefaultDiamondManager;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.env.PropertySourceLoader;
import org.springframework.core.env.PropertiesPropertySource;
import org.springframework.core.env.PropertySource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PropertiesLoaderUtils;

import java.io.IOException;
import java.io.StringReader;
import java.util.Properties;
import java.util.concurrent.Executor;

/**
 * Created by admin on 2016/9/20.
 */
public class DiamondPropertySourceLoader implements PropertySourceLoader {

    private static final Logger logger = LoggerFactory.getLogger(DiamondPropertySourceLoader.class);

    @Override
    public String[] getFileExtensions() {
        return new String[] { "properties", "xml" };
    }

    @Override
    public PropertySource<?> load(String name, Resource resource, String profile) throws IOException {
        Properties tt = PropertiesLoaderUtils.loadProperties(resource);
        String a = tt.getProperty("diamond.config");
        PropertySource<?> properties = null;
        if ("true".equalsIgnoreCase(a)) {
            String group = tt.getProperty("deploy.group");
            String dataId = tt.getProperty("deploy.dataId");
            logger.info("配置项：group=" + group);
            logger.info("配置项：dataId=" + dataId);
            if (!StringUtils.isEmpty(group) && !StringUtils.isEmpty(dataId)) {
                DiamondManager manager = new DefaultDiamondManager(group, dataId, new ManagerListener() {
                    public Executor getExecutor() {
                        return null;
                    }
                    public void receiveConfigInfo(String configInfo) {
                        logger.info("已改动的配置：\n" + configInfo);
                        StringReader reader = new StringReader(configInfo);
                        try {
                            DiamondPropertyConfigurer.props.load(reader);
                        } catch (IOException e) {
                            logger.error("配置改动错误",e);
                        }
                    }
                });
                try {
                    String configInfo = manager.getAvailableConfigureInfomation(1000);
                    logger.info("配置项内容: \n" + configInfo);
                    System.out.println("配置项内容: \n" + configInfo);
                    if (!StringUtils.isEmpty(configInfo)) {
                        String aa = new String(configInfo.getBytes(),"utf-8");
                        //System.out.println("配置项内容: \n" + aa);
                        StringReader reader = new StringReader(aa);
                        Properties props = new Properties();
                        props.load(reader);
                        DiamondPropertyConfigurer.load(props);
                        properties = new PropertiesPropertySource(name,props);
                    } else {
                        logger.error("在配置管理中心找不到配置信息");
                    }
                } catch (IOException e) {
                    logger.error("读取钻石配置出错",e);
                }
            }
        }
        return properties;
    }

}
