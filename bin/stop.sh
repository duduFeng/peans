#!/bin/bash
cd `dirname $0`
BIN_DIR=`pwd`
echo "BIN_DIR is $BIN_DIR"
cd $BIN_DIR/../webapp/WEB-INF
DEPLOY_DIR=`pwd`
CONF_DIR=$DEPLOY_DIR/classes

echo "DEPLOY_DIR is $DEPLOY_DIR"
echo "CONF_DIR is $CONF_DIR"

SERVER_NAME=`sed '/spring.application.name/!d;s/.*=//' classes/application.properties | tr -d '\r'`

if [ -z "$SERVER_NAME" ]; then
	SERVER_NAME=`hostname`
fi

echo "SERVER_NAME is $SERVER_NAME"

PIDS=`ps  --no-heading -C java -f --width 1000 | grep "$CONF_DIR" |awk '{print $2}'`
if [ -z "$PIDS" ]; then
    echo "ERROR: The $SERVER_NAME does not started!"
    exit 1
fi

if [ "$1" != "skip" ]; then
$BIN_DIR/dump.sh
fi

echo -e "Stopping the $SERVER_NAME ...\c"
for PID in $PIDS ; do
	kill $PID > /dev/null 2>&1
done

COUNT=0
while [ $COUNT -lt 1 ]; do    
    echo -e ".\c"
    sleep 1
    COUNT=1
    for PID in $PIDS ; do
		PID_EXIST=`ps --no-heading -p $PID`
		if [ -n "$PID_EXIST" ]; then
			COUNT=0
			break
		fi
	done
done
echo "OK!"
echo "PID: $PIDS"
