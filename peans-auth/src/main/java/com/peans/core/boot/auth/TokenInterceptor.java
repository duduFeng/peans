package com.peans.core.boot.auth;

import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class TokenInterceptor extends HandlerInterceptorAdapter {

    private TokerAuthExecutor tokerAuthExecutor;

    public TokenInterceptor(TokerAuthExecutor tokerAuthExecutor) {
        this.tokerAuthExecutor = tokerAuthExecutor;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
        HandlerMethod methodHandler = (HandlerMethod) handler;
        AuthExclude auth = methodHandler.getMethodAnnotation(AuthExclude.class);
        if (auth != null) {
            return true;
        }
        return tokerAuthExecutor.check(request,response);
    }
}
