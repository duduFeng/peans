package com.peans.core.boot.auth;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.peans.core.common.utils.AESKit;
import com.peans.core.protocol.RspVo;
import com.peans.core.protocol.RspVoFactory;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Component
public class TokerAuthExecutor {

    private final String key = "1234567890123456";

    private final Long timeOut=3000L;//过期时间

    private final String redisPreKey = "token:";

    @Resource
    private RedisTemplate<String, String> redisTemplate;


    public String signUp(UserSessionBean bean) {
        if (StringUtils.isBlank(bean.getUserId())) {
            throw new TokenException("userId为空");
        }
        String body = JSONObject.toJSONString(bean);
        redisTemplate.opsForValue().set(redisPreKey+bean.getUserId(),body);
        return AESKit.encrypt(bean.getUserId(),key);
    }

    public void signOut(HttpServletRequest request) {
        String userId = getUserId(request);
        redisTemplate.delete(redisPreKey+userId);
    }

    public <T extends UserSessionBean> T getCurrentUser(Class<T> classType,HttpServletRequest request){
        String jsonStr = getJsonBody(request);
        return  JSONObject.parseObject(jsonStr,classType);
    }

    private String getUserId(HttpServletRequest request) {
        //String token = request.getHeader("token");
        String token = request.getParameter("token");
        if (StringUtils.isBlank(token)) {
            // TODO: 2017/9/27 加上cookie判断
            throw new TokenException("token为空");
        }
        String userId = null;
        try {
            userId = AESKit.decrypt(token,key);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (userId == null) {
            throw new TokenException("token错误");
        }
        return userId;
    }

    private String getJsonBody(HttpServletRequest request) {
        String userId = getUserId(request);
        String json = redisTemplate.opsForValue().get(redisPreKey+userId);
        try {
            String str = new String(json.getBytes(),"utf-8");
            System.out.println(str);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        if (json == null) {
            throw new TokenException("token已失效");
        }
        return json;
    }

    public boolean check(HttpServletRequest request, HttpServletResponse response) {
        try {
            getJsonBody(request);
        } catch (Exception e) {
            e.printStackTrace();
            RspVo vo = RspVoFactory.fail("登录失败",null,null,e.getMessage());
            try {
                String str =  new String(JSON.toJSONString(vo).getBytes(),"utf-8");
                response.getWriter().write(str);
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            return false;
        }
        return true;
    }

}
