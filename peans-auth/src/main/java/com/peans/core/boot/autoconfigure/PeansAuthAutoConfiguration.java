package com.peans.core.boot.autoconfigure;

import com.peans.core.boot.auth.TokenInterceptor;
import com.peans.core.boot.auth.TokerAuthExecutor;
import com.peans.core.component.ReqParamsInterceptor;
import com.peans.core.component.RequestLogInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.nio.charset.Charset;

@Configuration
@ComponentScan(value = {"com.peans.core.boot.auth"})
public class PeansAuthAutoConfiguration extends WebMvcConfigurerAdapter {


    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        super.addInterceptors(registry);
        TokenInterceptor tokenInterceptor = new TokenInterceptor(getTokerAuthExecutor());
        registry.addInterceptor(tokenInterceptor);
        System.out.println("##################");
    }

    @Bean
    public TokerAuthExecutor getTokerAuthExecutor() {
        return new TokerAuthExecutor();
    }

    @Bean
    public StringRedisTemplate getRedisTemplate(RedisConnectionFactory factory) {
        StringRedisTemplate redisTemplate = new StringRedisTemplate(factory);
        redisTemplate.setDefaultSerializer(stringRedisSerializer());
        /*redisTemplate.setKeySerializer(stringRedisSerializer());
        redisTemplate.setValueSerializer(stringRedisSerializer());
        redisTemplate.setHashKeySerializer(stringRedisSerializer());
        redisTemplate.setHashValueSerializer(stringRedisSerializer());
        redisTemplate.afterPropertiesSet();*/
        return redisTemplate;
    }

    @Bean
    public StringRedisSerializer stringRedisSerializer() {
        return new StringRedisSerializer();
    }

    public static void main(String[] args) {
        String str = "{18,\"name\":\"hehe\",\"userId\":\"11\"}";
        byte[] ss = str.getBytes(Charset.forName("UTF8"));
        String str2 = new String(ss,Charset.forName("UTF8"));
        System.out.println(str2);
    }

}
