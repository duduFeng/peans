package com.peans.core.boot.auth;

import java.lang.annotation.*;

@Documented
@Target(ElementType.METHOD)
@Inherited
@Retention(RetentionPolicy.RUNTIME)
public @interface AuthExclude {
    //boolean check() default false;
}
