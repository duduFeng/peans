package com.peans.core.boot.auth;

public interface UserSessionBean  {

    String getUserId();

}
